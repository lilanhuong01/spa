<?php 
    namespace App\Helper;
    
    /**
     * summary
     */
    class Cart 
    {
        public $items = [];

        public function __construct()
        {
            $this->items = session()->get('cart');
            
        }

        public function add($product){
            if (isset($this->items[$product['id']])) {
                $this->items[$product['id']]['amount'] += 1;
            }else{
                $this->items[$product['id']] = $product;
                $this->items[$product['id']]['amount'] = 1;
            }
            session(['cart'=>$this->items]);
        }
        //xóa từng sản phẩm
        public function delete($id){
            if (isset($this->items[$id])) {
                unset($this->items[$id]);
            session(['cart'=>$this->items]);
            }
            
        }
        //xóa toàn bộ
        public function clear(){
            $this->items = [];
            session(['cart'=>$this->items]);
        }
        //update so luong
        public function update($id,$amo){
            $amo = is_numeric($amo) ? $amo : 1;

            $amo = $amo > 0 ? ceil($amo) : 1;
            if (isset($this->items[$id])) {
                $this->items[$id]['amount']=$amo;
                session(['cart'=>$this->items]);
            }
        }
        //tong $
        public function total(){
            $carts =$this->items;
            $tong = 0;
            if (count($carts)) {
                foreach ($carts as $cart) {
                    $tong = $tong + $cart['price']*$cart['amount'];
                }
            }
            return $tong;
        }
        //tong số lượng
        public function totalitem(){
            $carts = $this->items;
            $tong = 0;
            if (count($carts)) {
                foreach ($carts as $cart) {
                    $tong = $tong + $cart['amount'];
                }
            }
            return $tong;
        }
            
    }
 ?>