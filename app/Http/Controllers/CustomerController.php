<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
//giong $post
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Category;
use App\Models\Binhluan;
use View;
use Auth;

/**
 * summary
 */
class CustomerController extends Controller
{
     public $auth;
     public function __construct(){
        $this->auth = Auth::guard('customer');
      $cats = Category::where('parent',0)->get();
      View::share([
        'cats' => $cats
      ]);
    }
     public function formloginkh(Request $request){
        return view('custom.khachhang.loginkh');
     }
     public function loginkh(Request $request)
    {

        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ],
        [
            'username.requied' => 'Vui lòng nhập tài khoản',
            'password.required' => 'Vui lòng nhập mật khẩu'
        ]);
    	// User::create([
    	// 	'full_name' => 'admin demo',
    	// 	'username' => 'admin',
    	// 	'email' => 'huongkhung@gmail.com',
    	// 	'password' => bcrypt(123456),
    	// ]);
    	// die;
    	//echo '<pre>';
    	// Auth::attempt($request->only('username','password'),$request->has('remember'));
    	if(Customer::attempt($request->only('username','password'),$request->has('remember'))){
    		return redirect()->route('home.loginkh')->with('success','Chào mừng bạn trở lại');
    	}
    	else{
    		return redirect()->back()->with('error','Lỗi không đúng tài khoản');
    	}
    	//print_r(Auth::user());
    }
    

      public function dangky(Request $request)
    {

        $this->validate($request,[
            'name' => 'required',
            'password' =>'password',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required'
        ],
        [
            'name.requied' => 'Vui lòng nhập tên',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'email.requied' => 'Vui lòng nhập email',
            'address.requied' => 'Vui lòng nhập địa chỉ',
            'phone.requied' => 'Vui lòng nhập số điện thoại'
        ]);
        
    }
    public function comment(Request $request){

      $request->offsetunset('_token');
      if(Binhluan::create([
        'id_sp'=>$request->id_sp,
        'id_kh'=>$this->auth->user()->id,
        'comment'=>$request->comment,
        'status'=>0
      ])){
        return redirect()->back()->with('success','Cảm ơn bạn đã comment cho sản phẩm của shop');
      }
    }
    public function commentkh(){
      return view('frontend.comment');
    }


}

?>