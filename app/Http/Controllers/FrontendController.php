<?php 
namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use View;
use App\Helper\Cart;
use Illuminate\Http\Request;
use Auth;
use App\Models\Customer;
use App\Models\Binhluan;
use App\Models\Orders;
use App\Models\Orders_detail;
use App\Models\User;


/**
 * summary
 */
class FrontendController extends Controller
{
    
    public $auth;
    public function __construct(Cart $cart){
      $this->auth = Auth::guard('customer');
      $cats = Category::where('parent',0)->get();
       // $cats = Category::whereNotIn('image',0)->get();
      $products = Product::all();
      View::share([
        'cats' => $cats,
         'products' => $products,
          'auth' => $this->auth,
          'totalitem' => $cart->totalitem()
      ]);
    }
    public function index(Cart $cart)
    {
        $products = Product::paginate(8);
       // $products1 = Product::orderBy('id','DESC')->paginate(8);
        return view('frontend.index',[
          'products' => $products,
          'totalitem' => $cart->totalitem()
        ]);
    }
    public function newproduct(Cart $cart)
    {
       
        $products1 = Product::orderBy('id','DESC')->paginate(8);
        return view('frontend.newproduct',[
          'products1' => $products1,
          'totalitem' => $cart->totalitem()
        ]);
    }

    public function category($id)
    {
       $products = Product::where('catId',$id)->paginate(8);
        // $products = Product::simplePaginate(4);
       
        return view('frontend.category',[
          'products' => $products 
        ]);
    }
    public function view_product($slag)
    {
      //"select * from product where slag = $slag"
        $product = Product::where('slag',$slag)->first();
        //gửi dữ liệu qua frontend.product-view
        // Lấy bình luận theo id sanr phẩm
        $comments = Binhluan::where(['id_sp'=>$product->id,'status'=>1])->get();
        return view('frontend.product-view',[
          'product' => $product,
          'comments' => $comments,
        ]);
    }
   
      public function addcart($id){
        $product = Product::find($id)->toArray();

        $cart = new Cart;
        $cart->add($product);
        // dd(session()->get('cart'));
        return redirect()->route('home.huong-cart')->with('success','Bạn đã thêm giỏ hàng thành công');
       

    }
   public function viewcart(Cart $cart){
        return view('frontend.cart-view',[
            'carts'=>$cart->items,
            'total'=>$cart->total(),
            'totalitem'=>$cart->totalitem()
        ]);
    }
    public function deletecart($id){
        $cart = new Cart;
        $cart->delete($id);
        return redirect()->route('home.huong-cart')->with('success','Xóa thành công');
    }
    //xoa tat ca san pham
    public function clearcart(){
        $cart = new Cart;
        $cart->clear();
        return redirect()->route('home.huong-cart')->with('success','Xóa thành công');
    }
    //update san pham
    public function updatecart($id,Request $request){
        $cart = new Cart;
        $cart->update($id,$request->amount);
        return redirect()->route('home.huong-cart')->with('success','Cập nhật thành công');
    }


    //add tai khoan
    public function addkh(){
      return view('frontend.khachhang');
    }

    public function userkh(Request $request){

      $request->offsetunset('_token');
      if(Customer::create([
        'full_name'=>$request->full_name,
        'name'=>$request->name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'password'=>bcrypt($request->password)
      ])){
        return redirect()->route('home')->with('success','Them tai khoan thanh cong');
      }else {
         return redirect()->route('home.huong-addkh')->with('info','Them tai khoan that bai');
      }
    }
    public function adduser(){
      return view('frontend.user');
    }

    public function loginuser(Request $request){
      if($this->auth->attempt($request->only('email','password'),$request->has('remember'))){
        return redirect()->route('home')->with('success','Chuc mung dang nhap thanh cong');
      }
    else{
      return redirect()->back()->with('info','Dang nhap that bai');
    }
}
    public function logout(){
      $this->auth->logout();
      return redirect()->route('home')->with('success','Chuc mung thoat thanh cong');
    }
    //đặt hàng
    public function dathang(Cart $cart){
        return view('frontend.dathang',[
            'carts'=>$cart->items,
            'total'=>$cart->total(),
            'totalitem'=>$cart->totalitem()
        ]);
    }
    public function adddathang(Request $request,Cart $cart){
        if (count($cart->items)) {
            if ($hd = Orders::create([
                'id_kh' => $this->auth->user()->id,
                'sum_price' => $cart->total(),
                'status' => 0,
                'pay' => $request->pay,
                'ship' => $request->ship,
                'note' => $request->note
            ])) {
                foreach ($cart->items as $item) {
                    Orders_detail::create([
                        'id_hd' => $hd->id,
                        'id_sp' => $item['id'],
                        'price_product' => $item['price'],
                        'quantity' => $item['amount']
                    ]);
                }
                $cart->items = [];

                session(['cart'=>$cart->items]);
            }

            return redirect()->route('home')->with('success','Chúc mừng đặt hàng thành công');
        }else{
             return redirect()->back()->with('error','Có lỗi, kiểm tra lại giỏ hàng');
        }
    }
    public function hoadon(){
        return view('frontend.hoadon');
    }


    public function gioithieu()
    {
      return view('frontend.gioithieu');
    }

    public function tintuc()
    {
      return view('frontend.tintuc');
    }
    public function lienhe()
    {
      return view('frontend.lienhe');
    }

  public function search(Request $req){
     $products = Product::paginate(10);
    if ($req->key) {
      $products =  Product::where('name','LIKE','%'.$req->key.'%')
                            ->orWhere('price',$req->key)
                            ->paginate(10);
    }

    if ($req->size) {
      $products =  Product::where('size',$req->size)
                            ->paginate(10);
    }
    if ($req->price) {
      // dd($req->price);
      $price = explode('-', $req->price);
      if (isset($price[1])) {
         $products =  Product::where('price','>=',$price[0])
                            ->where('price','<=',$price[1])
                            ->paginate(10);
      }
     
      // dd($price);
    }
        // $products = Product::simplePaginate(4);
       
        return view('frontend.seach',[
          'products' => $products 
        ]);
    }
  
}
  ?>