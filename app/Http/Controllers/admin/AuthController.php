<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
//giong $post
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


/**
 * summary
 */
class AuthController extends Controller
{
    /**
     * summary
     */
    public function index()
    {
    	return view('admin.home.login');
    }
     public function login(Request $request)
    {

        $this->validate($request, array(
            'username' => 'required',
            'password' => 'required'
        ),
        [
            'username.requied' => 'Vui lòng nhập tài khoản',
            'password.required' => 'Vui lòng nhập mật khẩu'
        ]);
    	// User::create([
    	// 	'full_name' => 'admin demo',
    	// 	'username' => 'admin',
    	// 	'email' => 'huongkhung@gmail.com',
    	// 	'password' => bcrypt(123456),
    	// ]);
    	// die;
    	//echo '<pre>';
    	// Auth::attempt($request->only('username','password'),$request->has('remember'));
    	if(Auth::attempt($request->only('username','password'),$request->has('remember'))){
    		return redirect()->route('admin.index')->with('success','Chào mừng bạn trở lại');
    	}
    	else{
    		return redirect()->back()->with('error','Lỗi không đúng tài khoản');
    	}
    	//print_r(Auth::user());
    }
    public function add(){
        return view('admin.auth.add');
    }
    public function list(){
        // $users = User::all();
        return view('admin.auth.list',[
            'datas' => User::all()
        ]);
    } 
     public function authDelete($id){
        // User::destroy($id);
        if(Auth::User()->id == $id){
             return redirect()->route('admin.auth-list')->with('success','Không thể xóa tài khoản đang online');
        }else{
            User::find($id)->delete();
        return redirect()->route('admin.auth-list')->with('success','Xóa thành công');
        }
    }
     
    public function store(Request $request){
        $request->offsetunset('_token');

        if(User::create([
            'full_name' => $request->full_name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]))
        {
            return redirect()->route('admin.auth-list')->with('success','Tạo tài khoản thành công');
        }else{
            return redirect()->route('admin.auth-add')->with('error','Tạo tài khoản không thành công');
        }
    }
    public function edit($id)
    {
        /**
        * SELECT * FROM product WEHER id = $id;
        **/
       $user = User::find($id);
       /**
        * Đẩy dữ liêu qua view
        **/
       
       return view('admin.auth.edit',[
            'model' => $user,
            
       ]);
    }
    public function update($id,Request $request)
    {
        $model = User::find($id);
        
        if ($model->update([
           'full_name' => $request->full_name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ])) {
            return redirect()->route('admin.auth-list')->with('success','Thêm mới thành công');
        }else{
            return redirect()->back()->with('error','Thêm mới thất bại');
        }
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login')->with('error','Thoát thành công');
    }
}

?>