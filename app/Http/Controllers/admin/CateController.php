<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
//giong $post
use Illuminate\Http\Request;
use App\Models\Category;


/**
 * summary
 */
class CateController extends Controller
{
    /**
     * summary
     */
    public function index()
    {
    	return view('admin.home.login');
    }
    	
    public function add(){
        $cats = Category::all();
        return view('admin.category.add-cate',[
            'cats' => $cats
        ]);
    }
    public function list(){
        // $users = Category::all();
        return view('admin.category.cate-list',[
            'datas' => Category::all()
        ]);
    } 
    public function cateDelete($id){
        // User::destroy($id);
        Category::find($id)->delete();
        return redirect()->route('admin.cate-list')->with('success','Xóa thành công');
        
    }
    public function postAdd(Request $request){
        $request->offsetunset('_token');
        $image_name = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->move(base_path('uploads'),$file->getClientOriginalName());
            /**
            * Láy tên ảnh bằng đang uploads
            */
            $image_name = $file->getClientOriginalName();
        }
        


        if(Category::create([
            'name' => $request->name,
            'parent' => $request->parent,
            'status' => $request->status,
            'image' => $image_name
            
        ]))
        {
            return redirect()->route('admin.cate-list')->with('success','Tạo danh mục thành công');
        }else{
            return redirect()->route('admin.cate-add')->with('error','Tạo danh mục không thành công');
        }
    }
    public function edit($id)
    {
        /**
        * SELECT * FROM product WEHER id = $id;
        **/
       $danhmuc = Category::find($id);
       /**
        * Đẩy dữ liêu qua view
        **/
       
       return view('admin.category.edit',[
            'model' => $danhmuc,
            'cats' => Category::all(),
       ]);
    }
public function update($id,Request $request)
    {

         $model = Category::find($id);

        /**
        * Láy tên ảnh bằng ảnh cũ trong DB
        */
        $image_name = $model->image;

        /**
        * Check  nếu như chọn ảnh mới
        */
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->move(base_path('uploads'),$file->getClientOriginalName());
            /**
            * Láy tên ảnh bằng đang uploads
            */
            $image_name = $file->getClientOriginalName();
        }
        

        $model = Category::find($id);
        
        if ($model->update([
           'name' => $request->name,
            'parent' => $request->parent,
            'status' => $request->status,
            'image' => $image_name,
        ])) {
            return redirect()->route('admin.cate-list')->with('success','Thêm mới thành công');
        }else{
            return redirect()->back()->with('error','Thêm mới thất bại');
        }
    }
}

?>