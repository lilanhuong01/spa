<?php

namespace App\Http\Controllers\admin;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.product.index',[
            'datas' => Product::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.product.create',[
            'cats' => Category::all()
        ]);
    }
    public function proDelete($id){
         Product::destroy($id);
        //Product::find($id)->delete();
        return redirect()->route('admin.product')->with('success','Xóa thành công');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        $file->move(base_path('uploads'),$file->getClientOriginalName());
        if (Product::create([
            'name' => $request->name,
            'slag' => $request->slag,
            'descriptions' => $request->descriptions,
            'catId' => $request->catId,
            'price' => $request->price,
            'sale_price' => $request->sale_price,
            'status' => $request->status,
            'image' => $file->getClientOriginalName()
        ])) {
            return redirect()->route('admin.product')->with('success','Thêm mới thành công');
        }else{
            return redirect()->back()->with('error','Thêm mới thất bại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
        * SELECT * FROM product WEHER id = $id;
        **/
       $sanpham = Product::find($id);
       /**
        * Đẩy dữ liêu qua view
        **/
       
       return view('admin.product.edit',[
            'model' => $sanpham,
            'cats' => Category::all()
       ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $model = Product::find($id);

        /**
        * Láy tên ảnh bằng ảnh cũ trong DB
        */
        $image_name = $model->image;

        /**
        * Check  nếu như chọn ảnh mới
        */
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->move(base_path('uploads'),$file->getClientOriginalName());
            /**
            * Láy tên ảnh bằng đang uploads
            */
            $image_name = $file->getClientOriginalName();
        }
        
        
        if ($model->update([
            'name' => $request->name,
            'slag' => $request->slag,
            'descriptions' => $request->descriptions,
            'catId' => $request->catId,
            'price' => $request->price,
            'sale_price' => $request->sale_price,
            'status' => $request->status,
            'image' => $image_name
        ])) {
            return redirect()->route('admin.product')->with('success','Chỉnh sửa thành công');
        }else{
            return redirect()->back()->with('error','Chỉnh sửa thất bại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
