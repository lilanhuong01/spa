<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Binhluan extends Model
{
    protected $table='binhluan';

    protected $fillable = [
        'id_sp','id_kh', 'comment','status'
    ];
    // Lấy thông tin khách hàng bình luận
    public function khbl(){
    	return $this->hasOne('\App\Models\Customer','id','id_kh');
    }
   
}
