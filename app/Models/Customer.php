<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Customer extends Authenticatable
{
	 use Notifiable;
   protected $table = 'customer';
  protected $fillable = [
        'full_name','name', 'email','phone','address','password',
    ];
    
}
