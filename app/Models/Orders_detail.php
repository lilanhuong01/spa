<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders_detail extends Model
{
    protected $table ='Orders_detail';
    protected $fillable =[
    	'id_hd','id_sp','price_product','quantity'
    ];
}
