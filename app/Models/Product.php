<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table ='products';
    protected $fillable =[
    	'catId','name','slag','image','price','sale_price','status','descriptions'
    ];
}
