-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 04, 2018 lúc 05:19 PM
-- Phiên bản máy phục vụ: 10.1.26-MariaDB
-- Phiên bản PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `leoshop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `binhluan`
--

CREATE TABLE `binhluan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sp` int(11) NOT NULL,
  `id_kh` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `binhluan`
--

INSERT INTO `binhluan` (`id`, `id_sp`, `id_kh`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(1, 91, 1, 'Hello san pham gio qua', 0, '2017-12-22 12:04:05', '2017-12-22 12:04:05'),
(2, 91, 1, 'San pham chan qua', 1, '2017-12-22 12:04:39', '2017-12-22 12:04:39'),
(3, 91, 1, 'Hi hah hah aha', 1, '2017-12-22 12:10:31', '2017-12-22 12:10:31'),
(4, 91, 1, 'Hi hah hah aha', 1, '2017-12-22 12:10:31', '2017-12-22 12:10:31'),
(5, 4, 1, 'san pham tot lam', 0, '2018-01-03 10:24:51', '2018-01-03 10:24:51'),
(6, 4, 1, 'san pham tot lam', 1, '2018-01-03 10:24:51', '2018-01-03 10:24:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `parent`, `status`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Giày nam', 0, 1, NULL, '2017-12-11 15:28:12', '2017-12-11 15:28:12'),
(3, 'Giày lười nam', 2, 1, NULL, '2017-12-11 15:28:39', '2017-12-11 15:28:39'),
(5, 'Giầy da nam', 2, 1, NULL, '2017-12-11 15:35:52', '2017-12-11 15:35:52'),
(6, 'Giầy running', 2, 1, NULL, '2017-12-11 15:36:17', '2017-12-11 15:36:17'),
(7, 'Giầy football', 2, 1, NULL, '2017-12-11 15:36:53', '2017-12-11 15:36:53'),
(8, 'Giầy basketball', 2, 1, NULL, '2017-12-11 15:37:47', '2017-12-11 15:37:47'),
(9, 'Giày nữ', 0, 1, NULL, '2017-12-13 00:21:03', '2017-12-13 00:21:03'),
(11, 'Giày bệt', 9, 1, NULL, '2017-12-13 00:26:17', '2017-12-13 00:26:17'),
(12, 'Giày thể thao nữ', 9, 1, NULL, '2017-12-13 00:33:00', '2017-12-13 00:33:00'),
(14, 'Giày cao gót', 9, 1, NULL, '2017-12-13 00:35:48', '2017-12-13 00:35:48'),
(16, 'Giày Lười', 2, 1, NULL, '2017-12-18 16:19:49', '2017-12-18 16:19:49'),
(24, 'Danh mục ưa thích', 0, 1, '', '2017-12-18 18:22:02', '2017-12-18 18:22:02'),
(25, 'Giày Tây', 24, 1, 'banner_index_1.jpg', '2017-12-18 18:23:50', '2017-12-18 18:23:50'),
(26, 'Dép Sandal', 24, 1, 'banner_index_4.jpg', '2017-12-18 18:24:46', '2017-12-18 18:24:46'),
(27, 'Giày Lười', 24, 1, 'banner_index_2.jpg', '2017-12-18 18:25:14', '2017-12-18 18:25:14'),
(28, 'Giày Thể Thao', 24, 1, 'banner_index_3.jpg', '2017-12-18 18:25:46', '2017-12-18 18:25:46'),
(29, 'Áo thun', 24, 1, 'banner_index_6.jpg', '2017-12-18 22:28:15', '2017-12-18 22:28:15'),
(30, 'Áo sơ mi', 24, 1, 'banner_index_5.jpg', '2017-12-18 22:28:40', '2017-12-18 22:28:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `full_name`, `name`, `email`, `phone`, `password`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hương xinh gái', 'Hương', 'lilanhuong01@gmail.com', 968221530, '$2y$10$Wkr5DxMZ50iPtEHd.dHF1uAQuOLW2GSVqmkSZGvTpGOEqsIro6uXa', 'Hà Nội', 'jJe2iNaqkfDjPUvvm6l7ae8dPGGC0BKWNHZtcLc3wZMq5JBE2kILlwwDiFRN', '2017-12-21 12:55:10', '2017-12-21 12:55:10'),
(2, 'Hương xinh gái', 'Hương', 'lilanhuong01@gmail.com', 968221530, '$2y$10$dPzrzy.NYIAup2vN7kKeoeLczxN9JOIwod2CKMEzcjVY/8fWzDD7W', 'Hà Nội', NULL, '2017-12-21 12:55:10', '2017-12-21 12:55:10');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2014_10_12_000000_create_users_table', 1),
(9, '2014_10_12_100000_create_password_resets_table', 1),
(10, '2017_11_22_083821_create_user', 1),
(11, '2017_12_01_085419_create_table_product', 2),
(12, '2017_12_15_083800_create_table_customer', 3),
(13, '2017_12_15_083918_create_table_orders', 3),
(14, '2017_12_15_083946_create_table_orders_detail', 3),
(15, '2017_12_22_022719_create_binhluan_table', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_kh` int(11) NOT NULL,
  `sum_price` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `pay` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ship` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `id_kh`, `sum_price`, `status`, `pay`, `ship`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 1200000, 0, 'Tại cửa hàng', 'Tại cửa hàng', 'sss', '2017-12-21 18:48:12', '2017-12-21 18:48:12'),
(2, 1, 1200000, 0, 'Tại cửa hàng', 'Tại cửa hàng', 'sss', '2017-12-21 18:50:27', '2017-12-21 18:50:27'),
(3, 1, 620000, 0, 'Tại cửa hàng', 'Tại cửa hàng', 'df', '2017-12-21 23:07:51', '2017-12-21 23:07:51'),
(4, 1, 4850000, 0, 'Tại cửa hàng', 'Tại cửa hàng', 'sada', '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(5, 1, 4850000, 0, 'Tại cửa hàng', 'Tại cửa hàng', 'sada', '2017-12-22 00:23:48', '2017-12-22 00:23:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders_detail`
--

CREATE TABLE `orders_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_hd` int(11) NOT NULL,
  `id_sp` int(11) NOT NULL,
  `price_product` double(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders_detail`
--

INSERT INTO `orders_detail` (`id`, `id_hd`, `id_sp`, `price_product`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 2, 6, 650000.00, 1, '2017-12-21 18:50:27', '2017-12-21 18:50:27'),
(2, 2, 5, 550000.00, 1, '2017-12-21 18:50:27', '2017-12-21 18:50:27'),
(3, 3, 72, 310000.00, 2, '2017-12-21 23:07:51', '2017-12-21 23:07:51'),
(4, 4, 5, 550000.00, 3, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(5, 4, 68, 300000.00, 2, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(6, 4, 9, 650000.00, 2, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(7, 5, 5, 550000.00, 3, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(8, 4, 6, 650000.00, 2, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(9, 5, 68, 300000.00, 2, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(10, 5, 9, 650000.00, 2, '2017-12-22 00:23:48', '2017-12-22 00:23:48'),
(11, 5, 6, 650000.00, 2, '2017-12-22 00:23:48', '2017-12-22 00:23:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `catId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `descriptions` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `catId`, `name`, `slag`, `image`, `price`, `sale_price`, `status`, `descriptions`, `size`, `created_at`, `updated_at`) VALUES
(4, 2, 'Giày Nike cam2', 'giay-nike-cam-2', 'camfreetr6spectrumtrainingshoe-3cef5c47-80d3-454d-82cd-a2701293fff0 (1).jpg', 500000, 450000, 1, 'Giầy siêu nhẹ,,siêu ấm,đi lại ,chạy nhảy thoải mái', 0, '2017-12-13 13:20:29', '2017-12-20 12:50:28'),
(5, 2, 'Giày Nike hồng', 'giay-nike-hong', 'hongairzoomelite8runningshoe1 (1).jpg', 550000, 500000, 1, 'Giày siêu nhẹ,siêu ấm', 0, '2017-12-13 13:23:43', '2017-12-13 13:23:43'),
(6, 2, 'Giày Nike dometcon', 'giay-nike-dometcon', 'dotrainspeed4trainingshoe1 (1).jpg', 650000, 620000, 1, 'Giày đẹp,bền.siêu ấm', 0, '2017-12-13 13:25:31', '2017-12-13 13:25:31'),
(7, 2, 'Giày Nike dometconhong', 'giay-nike-dometcon-hong', 'hongairmax2017runningshoe1.jpg', 650000, 620000, 1, 'Giầy siêu bền,siêu ấm,siêu thoải mái', 0, '2017-12-13 13:26:57', '2017-12-13 13:26:57'),
(8, 2, 'Giày Nike hồng đen', 'giay-nike-hong-den', 'hongdenfreetr6printtrainingsho-4af1e0ed-369c-4534-a1f9-3da4df1bc67a (1).jpg', 670000, 650000, 1, 'Giầy bền ,đẹp ,rẻ,siêu êm', 0, '2017-12-13 13:33:41', '2017-12-13 13:33:41'),
(9, 2, 'Giày Nike tím xanh', 'giay-nike-tim-xanh', 'timairzoomstructure20runningsh-6d61447a-6bff-40a4-a962-6cec4a6c9a39 (1).jpg', 650000, 630000, 1, 'Giầy siêu đẹp,siêu bền,siêu ấm', 0, '2017-12-13 13:41:04', '2017-12-13 13:41:04'),
(10, 2, 'Giày Nike xanh', 'giay-nike-xanh', 'xanhduonglunartempo2runningsho-1ce74df5-7f8c-428e-a38c-66dcefd07a77 (1).jpg', 640000, 620000, 1, 'Giày đẹp,bền,êm chân', 0, '2017-12-13 13:45:52', '2017-12-13 13:45:52'),
(11, 2, 'Giày Nike xanh', 'giay-nike-xanh', 'xanhduonglunartempo2runningsho-1ce74df5-7f8c-428e-a38c-66dcefd07a77 (1).jpg', 640000, 620000, 1, 'Giày đẹp,bền,êm chân', 0, '2017-12-13 13:45:52', '2017-12-13 13:45:52'),
(14, 3, 'Giày lười mọi nam màu vàng', 'giay-luoi-moi-nam-mau-vang', 'kVbxTB_simg_b5529c_250x250_maxb.jpg', 355000, 179000, 1, 'Bạn không hài lòng có thể đổi trả', 0, '2017-12-13 14:32:19', '2017-12-13 14:32:19'),
(15, 3, 'Giày mọi nam Hàn Quốc caro', 'giay-moi-nam-han-quoc-caro', 'wqkBDa_simg_b5529c_250x250_maxb.jpg', 355000, 179000, 1, 'Bạn không hài lòng có thể đổi trả', 0, '2017-12-13 14:34:12', '2017-12-13 14:34:12'),
(16, 3, 'Giày mọi nam màu nâu đất thời trang', 'giay-moi-nam-mau-nau-dat-thoi-trang', 'Ng2vWk_simg_b5529c_250x250_maxb.jpg', 355000, 179000, 1, 'Bạn không hài lòng có thể đổi trả', 0, '2017-12-13 14:36:41', '2017-12-13 14:36:41'),
(17, 3, 'Giày lười quả chuông', 'giay-luoi-qua-chuong', 'giay-luoi-qua-chuong-thoi-trang-GNTA0279-D.jpg', 750000, 700000, 1, 'Bạn có thể đổi trả nếu không hài lòng về sản phẩm của chúng tôi', 0, '2017-12-13 17:13:47', '2017-12-13 17:13:47'),
(18, 3, 'Giày lười nam da bò cao cấp', 'giay-luoi-nam-da-bo-cao-cap', 'Giay-luoi-nam-da-bo-cao-cap-GNTA020-D.jpg', 700000, 680000, 1, 'Bạn có thể đổi trả nếu không hài lòng về sản phẩm của chúng tôi', 0, '2017-12-13 17:16:18', '2017-12-13 17:16:18'),
(19, 3, 'Giày lười nam GTNA 1200', 'giay-luoi-nam-gtna-1200', 'GIày-lười-nam-GNTA1908-D-4.jpg', 700000, 690000, 1, 'Thiết kế thời trang ,sang trọng', 0, '2017-12-13 17:21:26', '2017-12-13 17:21:26'),
(20, 3, 'Giày lười nam thời trang GNTA', 'giay-luoi-nam-gnta', 'Giầy-lười-nam-thời-trang-GNTA1908-N-2.jpg', 800000, 750000, 1, 'Thiết kế sang trọng ,lịch lãm', 0, '2017-12-13 17:27:44', '2017-12-13 17:27:44'),
(21, 6, 'Giày running adidat', 'giay-running-adidat', 'giay-running-adidas-alphabounce-em-nam-by4264.jpg', 3095000, 3050000, 1, 'thiết kế đẹp,kiểu dáng đẹp', 0, '2017-12-13 17:41:20', '2017-12-13 17:41:20'),
(22, 6, 'Giày running adidat cosmic', 'giay-running-adidat-cosmic', 'giay-running-adidas-cosmic-nam-bb4345.jpg', 1200000, 1186500, 1, 'thiết kế đẹp,bền', 0, '2017-12-13 17:45:46', '2017-12-13 17:45:46'),
(23, 6, 'Giày running adidat duramo', 'giay-running-adidat-duramo', 'giay-running-adidas-duramo-8-nam-ba8078.jpg', 1600000, 1595000, 1, 'siêu nhẹ,siêu bền', 0, '2017-12-13 17:47:38', '2017-12-13 17:47:38'),
(24, 6, 'Giày running adidat duramo xanh', 'giay-running-adidat-duramo-xanh', 'giay-running-adidas-duramo-8-nam-ba8079.jpg', 1600000, 1595000, 1, 'siêu đẹp,siêu bền', 0, '2017-12-13 17:49:07', '2017-12-13 17:49:07'),
(25, 6, 'Giày running adidat FLUID CLOUD', 'giay-running-adidat-fluid-cloud', 'giay-running-adidas-fluid-cloud-nam-bb3327.jpg', 1700000, 1596000, 1, 'siêu êm,siêu đẹp', 0, '2017-12-13 17:51:24', '2017-12-13 17:51:24'),
(26, 6, 'Giày running adidat FLUID CLOUD xanh', 'giay-running-adidat-fluid-cloud-xanh', 'giay-running-adidas-fluid-cloud-nam-bb3329.jpg', 1700000, 1595000, 1, 'siêu đẹp,siêu bền', 0, '2017-12-13 17:52:58', '2017-12-13 17:52:58'),
(27, 6, 'Giày running adidat GALAXY', 'giay-running-adidat-galaxy', 'giay-running-adidas-galaxy-3-nam-bb4361.jpg', 1320000, 1116000, 1, 'siêu êm,siêu đẹp', 0, '2017-12-13 17:54:44', '2017-12-13 17:54:44'),
(28, 6, 'Giày running nike anta', 'giay-running-nike-anta', 'giay-running-anta-nam-81715556-3.jpg', 1000000, 923000, 1, 'siêu ấm,siêu êm', 0, '2017-12-13 17:59:01', '2017-12-13 17:59:01'),
(29, 6, 'Giày running air max', 'giay-running-air-max', 'giay-running-nike-air-max-dynasty-2-nam-852430-003.jpg', 2150000, 1850000, 1, 'siêu đẹp,siêu êm', 0, '2017-12-13 18:01:03', '2017-12-13 18:01:03'),
(30, 6, 'Giày running nike AIR RELENTLESS', 'giay-running-nike-air-relentless', 'giay-running-nike-air-relentless-6-msl-nam-843881-011.jpg', 1990000, 1592000, 1, 'siêu đẹp', 0, '2017-12-13 18:03:21', '2017-12-13 18:03:21'),
(31, 6, 'Giày running nike AIR RELENTLESS 6', 'giay-running-nike-air-relentless-6', 'giay-running-nike-air-relentless-6-msl-nam-843881-402.jpg', 1190000, 1592000, 1, 'siêu đẹp,siêu bền', 0, '2017-12-13 18:05:59', '2017-12-13 18:05:59'),
(32, 6, 'Giày running nike AIR RELENTLESS 7', 'giay-running-nike-air-relentless-7', 'giay-running-nike-air-relentless-6-msl-nam-843881-403.jpg', 1990000, 1592000, 1, 'đẹp', 0, '2017-12-13 18:07:22', '2017-12-13 18:07:22'),
(33, 15, 'Giày Tây', 'giay-tay', 'banner_index_1.jpg', 1200000, 1160000, 1, 'giày tây đẹp', 0, '2017-12-18 16:32:26', '2017-12-18 16:32:26'),
(37, 25, 'Giày Tây thời trang', 'giay-tay-thoi-trang', 'D9T8z0_simg_b5529c_250x250_maxb.jpg', 1200000, 999000, 1, 'Giày đẹp', 0, '2017-12-18 23:12:34', '2017-12-18 23:12:34'),
(38, 25, 'Giày MonkStrap', 'giay-monkstrap', 'wqkBDa_simg_b5529c_250x250_maxb.jpg', 2100000, 2000000, 1, 'giày monkstrap', 0, '2017-12-18 23:42:05', '2017-12-18 23:42:05'),
(39, 5, 'Giày da nam công sở', 'giay-da-nam-cong-so', 'giay-da-nam-cong-so-da-bo-xin-db04-1508816583_avar.jpg', 2000000, 1590000, 1, 'Giày da nam công sở', 0, '2017-12-19 10:38:38', '2017-12-19 10:38:38'),
(40, 5, 'Giày da bò thật', 'giay-da-bo-that', 'giay-da-that-cao-cap-gucci-1482463345_avar.jpg', 2500000, 1500000, 1, 'Giày da bò thật', 0, '2017-12-19 10:40:23', '2017-12-19 10:40:23'),
(41, 5, 'Giày da cao cấp trung niên', 'giay-da-cao-cap-trung-nien', 'giay-da-nam-cao-cap-trung-nien-db03-1503899897_db02-banner.png', 2200000, 1490000, 1, 'Giày da cao cập trung niên', 0, '2017-12-19 10:42:10', '2017-12-19 10:42:10'),
(42, 5, 'Giày da nam Versace G', 'giay-da-nam-versace-g', 'giay-da-nam-versace-g-3942-1460597360_1.JPG', 5680000, 4530000, 1, 'Giày da nam versace G', 0, '2017-12-19 10:45:45', '2017-12-19 10:45:45'),
(43, 5, 'Giày da nam Salvatore Ferragamo G-A071', 'giay-da-nam-salvatore-ferragamo G-A071', 'giay-da-nam-salvatore-ferragamo-g-a071-1460601498_G-A071-1.jpg', 3260000, 2200000, 1, 'Giày da nam salvatore ferragamo G-A071', 0, '2017-12-19 10:49:16', '2017-12-19 10:49:16'),
(44, 7, 'Giày đá banh Nike', 'giay-da-banh-nike', '25396169_193133364434321_2404046144252379984_n-270x270.jpg', 400000, 390000, 1, 'Giày đá banh nike', 0, '2017-12-19 11:35:59', '2017-12-19 11:35:59'),
(45, 7, 'Giày đá banh Mererial', 'giay-da-banh-mererial', '23795705_187421415005516_7706586634270750954_n-270x270.jpg', 400000, 390000, 1, 'Giày đẹp', 0, '2017-12-19 11:37:22', '2017-12-19 11:37:22'),
(46, 7, 'Giày đá banh Nike mererial', 'giay-da-banh-nike-mererial', '23755039_187421608338830_617255949581364965_n-270x270.jpg', 410000, 390000, 1, 'Giày đẹp', 0, '2017-12-19 11:38:45', '2017-12-19 11:38:45'),
(47, 7, 'Giày đá banh Nike mererial victory', 'giay-da-banh-nike-mererial-victory', '23130870_182399352174389_4333901912706307403_n-270x270.jpg', 450000, 420000, 1, 'Giày đẹp', 0, '2017-12-19 11:42:43', '2017-12-19 11:42:43'),
(48, 7, 'Giày đá banh Nike mererial victory TF', 'giay-da-banh-nike-mererial-victory-tf', '22310635_179238655823792_6131819773662144894_n-270x270.jpg', 450000, 420000, 1, 'Giày đẹp', 0, '2017-12-19 11:44:03', '2017-12-19 11:44:03'),
(49, 7, 'Giày đá banh Nike mererial victory CF', 'giay-da-banh-nike-mererial-victory-cf', '22365664_178233319257659_3374047934475466868_n-270x270.jpg', 460000, 450000, 1, 'Giày đẹp', 0, '2017-12-19 11:46:49', '2017-12-19 11:46:49'),
(50, 7, 'Giày đá banh Nike Mercurial Neymar', 'giay-da-banh-nike-mercurial-neymar', '22221973_1915075345481651_4526171318896119917_n-270x270.jpg', 400000, 380000, 1, 'Giày đẹp', 0, '2017-12-19 11:49:10', '2017-12-19 11:49:10'),
(51, 7, 'Giày đá bóng Nike Hypervenom Neymar Jordan – Trắng đỏ', 'giay-da-banh-nike-hypervenom-neymar-jordan-trang-do', '50ee9ac6b49421aff5587a72bf34801583853388-270x270.jpg', 400000, 350000, 1, 'Giày đẹp', 0, '2017-12-19 11:51:30', '2017-12-19 11:51:30'),
(52, 7, 'Giày đá banh winbro-ads', 'giay-da-banh-winbro-ads', 'winbro-ads-270x270.jpg', 430000, 400000, 1, 'Giày đẹp', 0, '2017-12-19 11:54:46', '2017-12-19 11:54:46'),
(53, 7, 'Giay da bong Nike Hypervenom Neymar Jordan TF – Đen', 'giay-da-bong-nike-hypervenom-neymar-jordan-tf-den', '22365664_178233319257659_3374047934475466868_n-270x270.jpg', 400000, 370000, 1, 'Giày đẹp', 0, '2017-12-19 11:59:20', '2017-12-19 11:59:20'),
(54, 7, 'Giay da bong Nike Mercurial Victory 6 TF màu hồng trắng', 'giay-da-bong-nike-mercurial-victory-6-tf-mau-hong-trang', '22449710_179238729157118_5895175190586779933_n-270x270.jpg', 400000, 380000, 1, 'Giày đẹp', 0, '2017-12-19 12:01:14', '2017-12-19 12:01:14'),
(55, 7, 'Giay da bong Nike Mercurial Victory 6 TF màu hồng trắng', 'giay-da-bong-nike-mercurial-victory-6-tf-mau-hong-trang', '22449710_179238729157118_5895175190586779933_n-270x270.jpg', 400000, 380000, 1, 'Giày đẹp', 0, '2017-12-19 12:01:14', '2017-12-19 12:01:14'),
(56, 7, 'Giày đá bóng Nike Hypervenom II Neymar TF – Cam bạc', 'giay-da-bong-nike-hypervenom-ii-neymar-tf-cam-bac', 'C360_2016-07-14-10-18-12-910-270x270.jpg', 420000, 350000, 1, 'Giày đẹp', 0, '2017-12-19 12:03:12', '2017-12-19 12:03:12'),
(57, 7, 'Giày đá bóng Nike Hypervenom II Neymar TF – Cam bạc', 'giay-da-bong-nike-hypervenom-ii-neymar-tf-cam-bac', 'C360_2016-07-14-10-18-12-910-270x270.jpg', 420000, 350000, 1, 'Giày đẹp', 0, '2017-12-19 12:03:12', '2017-12-19 12:03:12'),
(58, 7, 'Giày đá bóng Nike Mercurial Victory 5 TF Xanh chuối', 'giay-da-bong-nike-mercurial-victory-5-tf-xanh-chuoi', 'nike4-270x270.jpg', 430000, 280000, 1, 'Giày đẹp', 0, '2017-12-19 12:06:09', '2017-12-19 12:06:09'),
(59, 7, 'Giày bóng đá Nike Mercurial Victory 6 TF Đỏ đen', 'giay-bong-da-nike-mercurial-victory-6-tf-do-den', 'giay-da-bong-nike-11-do-1-270x270.jpg', 390000, 355000, 1, 'Giày đẹp', 0, '2017-12-19 12:08:11', '2017-12-19 12:08:11'),
(60, 8, 'Peak Basketball E53361A – Xanh Đen', 'peak-basketball-e53361a-xanh-den', 'peak-basketball-e53361a-xanh-den-2-600x600.jpg', 800000, 780000, 1, 'Giày đẹp', 0, '2017-12-19 12:12:00', '2017-12-19 12:12:00'),
(61, 8, 'Peak Basketball E72371A – Ghi', 'peak-basketball-e72371a-ghi', 'peak-basketball-e72371a-ghi-2-600x600.jpg', 1200000, 1190000, 1, 'Giày đẹp', 0, '2017-12-19 12:14:03', '2017-12-19 12:14:03'),
(62, 8, 'Peak Basketball E53151A – Tím Vàng', 'peak-basketball-e53151a-tim-vang', 'peak-basketball-e53151a-tim-vang-2-600x600.jpg', 1200000, 1190000, 1, 'Giày đẹp', 0, '2017-12-19 12:15:36', '2017-12-19 12:15:36'),
(64, 8, 'Peak Dwight Howard II E64003A – Đen Đỏ', 'peak-dwight-howard-ii-e64003a-den-do', 'peak-dwight-howard-ii-den-do-2-600x600.jpg', 1300000, 1250000, 1, 'Gìay đẹp', 0, '2017-12-19 12:17:32', '2017-12-19 12:17:32'),
(65, 3, 'Giày lười nam công sở Sanvado da trơn màu đen (AP-031)', 'giay-luoi-nam-cong-so-sanvado-da-tron-mau-den-ap-031', 'giay-luoi-cong-so-sanvado-de-cao-da-tron-mau-den-pc-031-dc_2_.jpg', 1600000, 1550000, 1, 'Giày đẹp', 0, '2017-12-19 12:20:46', '2017-12-19 12:20:46'),
(66, 3, 'Giày lười nam công sở Sanvado da trơn màu đen (AP-021)', 'giay-luoi-nam-cong-so-sanvado-da-tron-mau-den-ap-021', 'giay-luoi-nam-cong-so-sanvado-da-tron-mau-den-ap-031_7_.jpg', 1000000, 800000, 1, 'Giày đẹp', 0, '2017-12-19 12:23:14', '2017-12-19 12:23:14'),
(67, 11, 'Aliza570', 'aliza-570', 'thumb_1513584155.jpg', 300000, 280000, 1, 'Giày bệt', 0, '2017-12-19 14:02:44', '2017-12-19 14:02:44'),
(68, 11, 'Aliza568 Giày bệt nữ Aliza', 'aliza-568-giay-bet-nu-aliza', 'thumb_1512892209.jpg', 300000, 280000, 1, 'Giày bệt nữ', 0, '2017-12-19 14:04:18', '2017-12-19 14:04:18'),
(69, 11, 'Giày bệt', 'giay-bet', 'thumb_1512891962.jpg', 250000, 220000, 1, 'Giày bệt', 0, '2017-12-19 14:05:30', '2017-12-19 14:05:30'),
(70, 11, 'Giày bệt Aliza', 'giay-bet-aliza', 'thumb_1509369609.jpg', 320000, 270000, 1, 'Giày bệt', 0, '2017-12-19 14:06:59', '2017-12-19 14:06:59'),
(71, 11, 'Giày bệt Alizaven', 'giay-bet-alizaven', 'thumb_1509359775.jpg', 300000, 270000, 1, 'Giày bệt', 0, '2017-12-19 14:08:24', '2017-12-20 12:51:02'),
(72, 11, 'Giày bệt Aliza2', 'giay-bet-aliza-2', 'thumb_1509359314.jpg', 310000, 280000, 1, 'Giày bệt', 0, '2017-12-19 14:09:50', '2017-12-20 12:52:40'),
(73, 11, 'Giày bệt Aliza3', 'giay-bet-aliza-3', 'thumb_1495063879.jpg', 300000, 270000, 1, 'Giày bệt', 0, '2017-12-19 14:11:04', '2017-12-20 12:53:18'),
(74, 11, 'Giày bệt VNXK Aliza91353B da bò', 'giay-bet-vnxk-aliza-91353b-da-bo', 'thumb_1441190950.jpg', 290000, 265000, 1, 'Giày vnxk', 0, '2017-12-19 14:13:15', '2017-12-19 14:13:15'),
(75, 11, 'Giày bệt mềm', 'giay-bet-mem', 'thumb_1500071607.jpg', 340000, 290000, 1, 'Giày bệt mềm', 0, '2017-12-19 14:18:45', '2017-12-19 14:18:45'),
(76, 11, 'Giày bệt mềm', 'giay-bet-mem', 'thumb_1500071607.jpg', 340000, 290000, 1, 'Giày bệt mềm', 0, '2017-12-19 14:18:45', '2017-12-19 14:18:45'),
(77, 11, 'Giày bệt Aliza4', 'giay-bet-aliza-4', 'thumb_1493298677.jpg', 320000, 310000, 1, 'giày bệt mềm', 0, '2017-12-19 14:20:25', '2017-12-20 12:53:46'),
(78, 11, 'Giày bệt Aliza', 'giay-bet-aliza-5', 'thumb_1502045777.jpg', 345000, 330000, 1, 'Giày bệt nữ aliza', 0, '2017-12-19 14:25:03', '2017-12-20 12:54:07'),
(79, 12, 'Giày Sneaker thời trang nữ Zapas – Gn015 (Hồng)', 'giay-sneaker-thoi-trang-nu-zapas-gn015-hong', '1796182_M.jpg', 450000, 230000, 1, 'GIày nữ đẹp', 0, '2017-12-19 14:28:59', '2017-12-19 14:28:59'),
(80, 12, 'Giày sneaker thời trang nữ Zapas', 'giay-sneaker-thoi-trang-nu-zapas', '1827327_M.jpg', 350000, 210000, 1, 'Giày nữ thời trang', 0, '2017-12-19 14:30:53', '2017-12-19 14:30:53'),
(81, 12, 'Giày thể thao Biti\'s Hunter nữ - BST', 'giay-the-thao-bitis-hunter-nu-bst', '1824471_M.png', 600000, 580000, 1, 'GIầy đẹp', 0, '2017-12-19 14:32:18', '2017-12-19 14:32:18'),
(84, 12, 'Giày thể thao nữ Bitis cao cấp (Đỏ) - DSM062633DOO', 'giay-the-thao-nu-bitis-cao-cap-do-dsm062633d00', '1785673_M.jpg', 750000, 700000, 1, 'Giày thể thao đjep', 0, '2017-12-19 14:33:52', '2017-12-19 14:33:52'),
(85, 12, 'Giày thể thao nam nữ Nike Roshe Run 511882-111', 'giay-the-thao-nu-nike-boshe-run-511882', '1867559_M.png', 2280000, 1890000, 1, 'Giày đẹp', 0, '2017-12-19 14:35:43', '2017-12-19 14:35:43'),
(86, 12, 'Giày sneaker thể thao nữ Zapas trắng - GS011WH', 'giay-sneaker-the-thao-nu-zapas-trang-gs011wh', '1843893_M.png', 299000, 199000, 1, 'Giày đẹp', 0, '2017-12-19 14:37:33', '2017-12-19 14:37:33'),
(87, 12, 'Giày thể thao nữ Anta 82718850-2 màu hồng', 'giay-the-thao-nu-anta-82718850-2-mau-hong', '1830501_M.png', 1640000, 1600000, 1, 'Giày đẹp', 0, '2017-12-19 14:40:10', '2017-12-19 14:40:10'),
(88, 12, 'Giày thể thao Bitis nữ Hunter (Đen)', 'giay-the-thao-bitis-nu-hunter-den', '1758133_M.png', 750000, 530000, 1, 'Giày đẹp', 0, '2017-12-19 14:41:46', '2017-12-19 14:41:46'),
(90, 12, 'Giày nữ thể thao AZ79 màu trắng - WNTT0135001A1', 'giay-nu-the-thao-az79-mau-trang-wntt0135001a1', '1727912_M.png', 350000, 285000, 1, 'Giày đẹp', 0, '2017-12-19 14:43:09', '2017-12-19 14:43:09'),
(91, 12, 'Giày thể thao nữ xanh đậm - WNTT0021012A1', 'giay-the-thao-nu-xanh-dam-wntt0021012a1', '1689039_M.png', 379000, 260000, 1, 'Giày đẹp', 0, '2017-12-19 14:44:49', '2017-12-19 14:44:49'),
(93, 12, 'Giày thể thao nữ màu đen - WNTT0021017A1', 'giay-the-thao-nu-mau-den-wntt0021017a1', '1689049_M.png', 379500, 189500, 1, 'Giày đẹp', 0, '2017-12-19 14:46:13', '2017-12-19 14:46:13'),
(94, 12, 'Giày Slip-on Nữ QuickFree Lightly Synthetic - W160101-001', 'giay-slip-on-nu-quickfree-lightly-synthetic-w160101-001', '1666521_M.png', 420000, 399000, 1, 'Giày slip-on đẹp', 0, '2017-12-19 14:47:48', '2017-12-19 14:47:48'),
(95, 12, 'Giày Paperplanes Unisex màu đen vàng thời trang - PP 1336', 'giay-paperplanes-unisex-mau-den-vang-thoi-trang-pp-1336', '1615073_M.png', 1162500, 1104000, 1, 'Giày đẹp', 0, '2017-12-19 14:50:29', '2017-12-19 14:50:29'),
(96, 12, 'Giày sneaker thể thao nữ Ecko Unltd màu đỏ IF17-26118', 'giay-sneaker-the-thao-nu-ecko-unltd-mau-do-if-17-26118', '1877777_M.jpg', 1200000, 1150000, 1, 'Giày đẹp', 0, '2017-12-19 14:52:19', '2017-12-19 14:52:19'),
(97, 12, 'Giày sneaker thể thao nữ Ecko Unltd màu xanh dương', 'giay-sneaker-the-thao-nu-eko-unltd-mau-xanh-duong', '1877786_M.jpg', 1200000, 1180000, 1, 'Giày đẹp', 0, '2017-12-19 14:53:46', '2017-12-19 14:53:46');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `full_name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Lý Lan Hương', 'admin', 'lilanhuong01@gmail.com', '$2y$10$4zzTwrh604tnd0ajumewcuRO61WjbtvbCOK.emZgPb1Iumw5fEmna', 'rLfycobVbA5jR7nFbYl517S0qU5rfZV57spGxTJhmoRZMadZYwfvG2H3ydvF', '2017-12-06 18:13:01', '2017-12-06 18:13:01'),
(2, 'Hương xinh gái', 'huong', 'huong@gmail.com', '$2y$10$UPpT/GM/SU373hI2gA.CCOYk0RDWEEDsVZlXsJF8XeUBtVeRz05nK', NULL, '2017-12-08 11:12:14', '2017-12-08 15:16:19');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
