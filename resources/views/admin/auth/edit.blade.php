@extends('admin.layouts.backend')

@section('title','Chỉnh sửa người dùng')
@section('sub-title',$model->full_name)
@section('content')
<form action="" method="POST" role="form">
	
	<div class="form-group">
		<label for="">Fullname</label>
		<input type="text" class="form-control" name="full_name" placeholder="Input field" value="{{$model->full_name}}">
	</div>
		<div class="form-group">
		<label for="">Username</label>
		<input type="text" class="form-control" name="username" placeholder="Input field" value="{{$model->username}}">
	</div>
		<div class="form-group">
		<label for="">Email</label>
		<input type="text" class="form-control" name="email" placeholder="Input field" value="{{$model->email}}">
	</div>
		<div class="form-group">
		<label for="">Password</label>
		<input type="text" class="form-control" name="password" placeholder="Input field" value="{{$model->password}}">
	</div>

	
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
@stop()