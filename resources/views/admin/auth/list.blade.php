@extends('admin.layouts.backend')

@section('content')

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Username</th>
			<th>Email</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	@foreach($datas as $model)
		<tr>
			<td>{{$model->id}}</td>
			<td>{{$model->username}}</td>
			<td>{{$model->email}}</td>
			<td>
				<a href="{{route('admin.auth-edit',['id'=>$model->id])}}" title="Xem" class="label label-success">Sửa</a>
				@if(Auth::user()->id == $model->id)

				@else
				<a href="{{route('admin.auth-delete',['id'=>$model->id])}}" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
				@endif
				
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
@stop()

@section('title','Danh sách người dùng')
@section('sub-title','các tài khoản quản trị')