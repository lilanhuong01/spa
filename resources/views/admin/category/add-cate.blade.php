@extends('admin.layouts.backend')

@section('content')
<form action="" method="POST" role="form" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="">Tên danh mục</label>
		<input type="text" class="form-control" name="name" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Danh mục cha</label>
		<select name="parent" id="inputParent" class="form-control" required="required">
			<option value="0">Không có</option>
			@foreach($cats as $cat)
			<option value="{{$cat->id}}">{{$cat->name}}</option>
			@endforeach
		</select>
	</div>
		<div class="form-group">
		<label for="">Trạng thái</label>
		<input type="text" class="form-control" name="status" placeholder="Input field">
	</div>
	<div class="form-group">
		<label for="">Ảnh danh mục</label>
		<input type="file" class="form-control" name="image" placeholder="Input field">
	</div>

	
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
@stop()