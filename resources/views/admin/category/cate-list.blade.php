@extends('admin.layouts.backend')

@section('content')

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Parent</th>
			<th>Status</th>
			<th>Image</th>
		</tr>
	</thead>
	<tbody>
	@foreach($datas as $model)
		<tr>
			<td>{{$model->id}}</td>
			<td>{{$model->name}}</td>
			<td>{{$model->parent}}</td>
			<td>{{$model->status}}</td>
			<td>{{$model->image}}</td>
			<td>
				<a href="{{route('admin.cate-edit',['id'=>$model->id])}}" title="Sửa" class="label label-success">Sửa</a>
				<a href="{{route('admin.cate-delete',['id'=>$model->id])}}" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
@stop()

@section('title','Danh sách danh mục')
@section('sub-title','các danh mục sản phẩm')