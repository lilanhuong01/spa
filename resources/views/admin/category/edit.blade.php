@extends('admin.layouts.backend')
@section('title','Chỉnh sửa danh mục')
@section('sub-title',$model->name)
@section('content')
<form action="" method="POST" role="form">
	
	<div class="form-group">
		<label for="">Tên danh mục</label>
		<input type="text" class="form-control" name="name" placeholder="Input field" value="{{$model->name}}">
	</div>
		<div class="form-group">
		<label for="">Danh mục cha</label>
		<select name="parent" id="inputParent" class="form-control" required="required">
			<option value="0">Không có</option>
			@foreach($cats as $cat)
			<option value="{{$cat->catId}}">{{$cat->name}}</option>
			@endforeach
		</select>
	</div>
		<div class="form-group">
		<label for="">Trạng thái</label>
		<input type="text" class="form-control" name="status" placeholder="Input field" value="{{$model->status}}">
	</div>
	<div class="form-group">
		<label for="">Ảnh danh mục</label>
		<input type="file" class="form-control" name="image" placeholder="Input field" value="{{$model->image}}">
	</div>

	
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
@stop()