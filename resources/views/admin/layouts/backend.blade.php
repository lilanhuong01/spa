
  @include('admin.layouts.backend-header')
  @include('admin.layouts.backend-aside')
  <div class="content-wrapper">
    @include('admin.layouts.alert')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('title')
        <small>@yield('sub-title')</small>
      </h1>
      <ol class="breadcrumb">
       @yield('breadcrumb')
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="btn-too-bar pull-left">
                  @yield('tool-bar')
              </div>
              <div class="box-tools pull-right">
                <form action="" method="GET" class="form-inline">
                <div class="input-group form-group">
                  <input type="text" name="search" class="form-control" placeholder="Từ khóa tìm kiếm..." value="{{old('search')}}" />
                  <span class="input-group-btn">
                    <button type="submit"  id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body">
          
            <div class="panel panel-default">
            <div class="panel-body">
              @yield('content')
            </div>
          </div>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          @yield('footer')
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
@include('admin.layouts.backend-footer')