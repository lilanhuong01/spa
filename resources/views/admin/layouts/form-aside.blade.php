
  @include('layouts.backend-header')
  @include('layouts.backend-aside')
  <div class="content-wrapper">
    @include('layouts.alert')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('title')
        <small>@yield('sub-title')</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <form action="@yield('form-action')" method="POST">
        <div class="box-header with-border">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="btn-too-bar pull-left">
                  @yield('tool-bar')
              </div>
              <div class="box-tools pull-right">
                <ol class="breadcrumb">
                 @yield('breadcrumb')
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body">
            <div class="col-sm-8">
              <div class="row">
                <div class="panel panel-default">
                  <div class="panel-body">
                    @yield('left')
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="row">
                <div class="panel panel-default">
                  <div class="panel-body">
                    @yield('right')
                  </div>
                </div>
              </div>
            </div>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          @yield('footer')
        </div>
        <!-- /.box-footer-->
        <input type="hidden" name="_token" value="{{csrf_token()}}" />
      </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
@include('layouts.backend-footer')