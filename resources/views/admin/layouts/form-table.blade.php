
  @include('layouts.backend-header')
  @include('layouts.backend-aside')
  <div class="content-wrapper">
    @include('layouts.alert')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @yield('title')
        <small>@yield('sub-title')</small>
      </h1>
      <ol class="breadcrumb table-form">
       <form action="" method="GET" class="form-inline">
        <li>
          <div class="input-group form-group">
            <input type="text" name="search" class="form-control" placeholder="Từ khóa tìm kiếm..." value="{{old('search')}}" />
            <span class="input-group-btn">
              <button type="submit"  id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
      </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-body">
              <div class="row">
               <div class="col-md-4">
                <form action="@yield('form-action')" method="POST">
                  
                    <div class="panel panel-default">
                      <div class="panel-body">
                        @yield('tool-bar')
                      </div>
                    </div>
                  <div class="panel panel-default">
                      <div class="panel-body">
                        @yield('left')
                      </div>
                    </div>
                  
                  <input type="hidden" name="_token" value="{{csrf_token()}}" />
                </form>
               </div>
               <div class="col-md-8">
                <form action="@yield('table-form-action')" method="POST">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="box-tools pull-left">
                      @yield('table-tool-bar')
                    </div>
                    <div class="box-tools pull-right">
                      <ol class="breadcrumb">
                       @yield('breadcrumb')
                      </ol>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-body">
                    @yield('right')
                  </div>
                </div>
                @if(isset($datas))
                <div class="panel panel-default panel-pagination">
                  <div class="panel-body">
                    {{ $datas->appends(Request::only('search'))->links() }}
                  </div>
                </div>
                @endif
               </div>
               <input type="hidden" name="_token" value="{{csrf_token()}}" />
              </form>
              </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          @yield('footer')
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @include('layouts.backend-footer')