@extends('admin.layouts.backend')

@section('title','Chỉnh sửa sản phẩm')
@section('sub-title',$model->name)
@section('content')
<form action="" method="post" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-9">
			<div class="form-group">
				<label for="">Tên sản phẩm</label>
				<input type="text" class="form-control" name="name" placeholder="Tên sản phẩm..." value="{{$model->name}}" />
			</div>
			<div class="form-group">
				<label for="">Đường dẫn tĩnh</label>
				<input type="text" class="form-control" name="slag" placeholder="Đường dẫn tĩnh..." value="{{$model->slag}}"/>
			</div>
			<div class="form-group">
				<label for="">Ảnh sản phẩm</label>
				<input type="file" name="image"  />
				<img src="{{url('uploads').'/'.$model->image}}" width="100">
			</div>
			<div class="form-group">
				<label for="">Mô tả sản phẩm</label>
				<textarea name="descriptions" class="form-control" rows="3" placeholder="Mô tả sản phẩm">{{$model->descriptions}}</textarea>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Danh mục</label>
				<select name="catId"  class="form-control" required>
					<option value="">Chọn danh mục</option>
				@if($cats->count()) @foreach($cats as $cat)
				@php 
					$selected = $cat->id == $model->catId ? 'selected' : '';
				@endphp
					<option value="{{$cat->id}}" {{$selected}}>{{$cat->name}}</option>
				@endforeach @endif
				</select>
			</div>
			<div class="form-group">
				<label for="">Giá</label>
				<input type="text" class="form-control" name="price" placeholder="Tên sản phẩm..." value="{{$model->price}}"/>
			</div>
			<div class="form-group">
				<label for="">Giá sale</label>
				<input type="text" class="form-control" name="sale_price" placeholder="Tên sản phẩm..." value="{{$model->sale_price}}"/>
			</div>
			<div class="form-group">
				<label for="">Trạng thái</label>
				<select name="status"  class="form-control">
					<option value="0" @if($model->status == 0) selected @endif>Ẩn</option>
					<option value="1" @if($model->status == 1) selected @endif>Hiển thị</option>
				</select>
			</div>
		</div>
	</div>
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<button type="submit" class="btn btn-primary">CHỉnh sửa</button>
</form>
@stop