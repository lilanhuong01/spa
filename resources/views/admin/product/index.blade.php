@extends('admin.layouts.backend')

@section('title','Quản lý sản phẩm')
@section('content')
<table class="table table-hover">
	<thead>
		<tr>
			<th>Ảnh</th>
			<th>Tên sản phẩm</th>
			<th>Ngày tạo</th>
			<th>Đường Dẫn</th>
			<th>Trạng thái</th>
			
		</tr>
	</thead>
	<tbody>
	@if($datas->count()) @foreach($datas as $model)
		<tr>
			<td>
				<img src="{{url('uploads').'/'.$model->image}}" width="80" />
			</td>
			<td>{{$model->name}}</td>
			<td>{{$model->created_at}}</td>
			<td>{{$model->slag}}</td>
			<td>{{$model->status}}</td>
			<td>
				<a href="{{route('admin.product-edit',['id'=>$model->id])}}" title="Sửa" class="label label-success">
					<i class="fa fa-save"></i> Sửa</a>
					<a href="{{route('admin.product-delete',['id'=>$model->id])}}" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
			</td>
		</tr>
	@endforeach @endif
	</tbody>
</table>
@stop