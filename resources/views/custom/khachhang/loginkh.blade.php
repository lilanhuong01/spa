@extends('layouts.layoutkh')
@section('content')
<div class="container">
<div class="col-md-5">
<form action="{{route('home.loginkh')}}" method="post">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Tài khoản">

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if($errors->has('username'))
          <strong><font color='red'>Vui lòng nhập tài khoản</font></strong>
        @endif
      </div>

      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          @if($errors->has('password'))
          <strong><font color='red'>Vui lòng nhập mật khẩu</font></strong>
        @endif
      </div>
      <div class="form-group has-feedback">
        <div class="col-xs-7">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> Ghi nhớ mật khẩu
            </label>
          </div>
        </div>
      <!-- /.col -->
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Đăng nhập</button>
        </div>
      <!-- /.col -->
      </div>
      
      
      
    </form>
</div>
</div>
@stop()
