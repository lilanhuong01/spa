@extends('layouts.main-layout')

@section('giohang')
	<li class="checkout"><a href="{{route('home.huong-cart')}}">Giỏ Hàng</a>
@stop()


<!-- @section('category')

 	  <div class="categories col-md-3">
    <h3>DANH MỤC SẢN PHẨM</h3>
    <ul>
    @foreach($cats as $cat)
      <li><a href="{{route('home.category',['id'=>$cat->id])}}">{{$cat->name}}</a></li> 
      @endforeach
      </ul>
  </div>
 @stop()
  @section('sanpham')

 @foreach($cats as $cat)

 	

 		<li class=""><a href="{{route('home.category',['id'=>$cat->id])}}">{{$cat->name}}</a></li>
      
 @endforeach
     
 @stop()

 -->
@section('content')
<div class="container">
	<div class="col-md-3" style="border-right: 1px solid black">
		<h4 style="color: red">DANH MỤC SẢN PHẨM</h4>
		@foreach($cats as $cat)
      		<li style="border-bottom: 1px dotted red"><a href="{{route('home.category',['id'=>$cat->id])}}">{{$cat->name}}</a></li> 
			@foreach($cat->childs as $c)
		        <li class="current" style="border-bottom: 1px dotted red; padding-top: 5px">
		            <a href="{{route('home.category',['id'=> $c->id])}}" class="current" title="Lifestyle">{{$c->name}}</a>
		        </li>
		    @endforeach()


      	@endforeach
	</div>
<div class="col-md-9">
<table class="table table-hover" >
	<thead>
		<tr>
			<th width="10">STT</th>
			<th width="10">Ảnh</th>
			<th width="10">Tên Sản Phẩm</th>
			<th width="10">Giá</th>
			<th width="10">Số Lượng</th>
			<th width="10">Size</th>
			<th width="10">Tác Vụ</th>
		</tr>
	</thead>
	<tbody>
		@php($n = 1)
		@if(count($carts)) @foreach($carts as $cart)
		<tr>
			<td>{{$n}}</td>
			<td  width="10">
				<img src="{{url('uploads')}}/{{$cart['image']}} "  width="70" >
			</td>
			<td>{{$cart['name']}}</td>
			<td>{{number_format($cart['price'],0,'',',')}}<sup style="color: red">đ</sup></td>
			<td>
				<form action="{{route('home.huong-updatecart',['id'=>$cart['id']])}}" method="POST" role="form">
						<input type="number" name="amount" value="{{$cart['amount']}}">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
					<button type="submit" class="btn-xs btn btn-success">Cập Nhật</button>
				</form>
			</td>
			<td>
				<a href="{{route('home.deletecart',['id'=>$cart['id']])}}"  class="btn-xs btn btn-danger" onclick="return confirm('Bạn có muốn xóa không ?')">Xóa</a>
			</td>
		</tr>
		@php($n ++)
	@endforeach @endif
	</tbody>
	<div>
		<tr>
			<th colspan="3">Tổng số</th>
			<th>{{number_format($total,0,'',',')}} đ</th>
			<th colspan="2">{{$totalitem}} SP	</th>
		</tr>
	</div>
</table>
<div class="text-center">
	<a href="{{route('home')}}" title="" class="btn btn-success">Tiếp tục mua hàng</a>
	<a href="{{route('home.huong-clearcart')}}" title="" class="btn btn-danger" onclick="return confirm('Bạn có muốn xóa không ?')">Xóa giỏ hàng</a>
	<a href="{{route('home.dathang')}}" title="" class="btn btn-warning" >Đặt Hàng</a>
</div>
</div>
</div>
@stop()

