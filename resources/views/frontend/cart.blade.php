@extends('layouts.main-layout')

@section('content')
	<div class="row">
		<div class="col-md-5">
			<img src="{{url('uploads').'/'.$product->image}}" class="img-responsive">
		</div>
		<div class="col-md-7">
			<h3>{{$product->name}}</h3>
			<strong>Giá:</strong><del>{{$product->price}}</del> - <span>{{$product->sale_price}}</span>
			<p>
				<strong>Chi tiết:</strong>{{$product->descriptions}}
			</p>
			
		</div>
	</div>
@stop()