
@extends('layouts.main-layout')

@section('content')
<div class="col-md-5">
<form action="" method="POST" role="form">
	<legend>Bình luận sản phẩm</legend>

	<div class="form-group">
		<label for="">Sản phẩm:</label>
		<input type="text" class="form-control" name="id_sp" placeholder="Input field">
	</div>
	<div class="form-group">
		<label for="">Tên khách hàng:</label>
		<input type="text" class="form-control" name="id_kh" placeholder="Input field">
	</div>
	<div class="form-group">
		<label for="">Bình luận:</label>
		<input type="text" class="form-control" name="comment" placeholder="Input field">
	</div>
	<div class="form-group">
		<label for="">Trạng thái:</label>
		<input type="text" class="form-control" name="status" placeholder="Input field">
	</div>

	

	<button type="submit" class="btn btn-primary">Bình luận</button>
</form>
</div>
$stop()