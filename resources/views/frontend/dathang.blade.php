@extends('layouts.main-layout')

@section('content')
    <div class="col-md-5">
    @if($auth->check())
        @if(count($carts))
            <form action="" method="POST" role="form">
            <legend style="text-align: center;">Đặt Hàng</legend>
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Nhập Name" value="{{$auth->user()->name}}">
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Nhập Email" value="{{$auth->user()->email}}">
            </div>
            <div class="form-group">
                <label for="">Phone</label>
                <input type="text" class="form-control" name="phone" placeholder="Nhập Số Điện Thoại" value="{{$auth->user()->phone}}">
            </div>
            <div class="form-group">
                <label for="">Address</label>
                <input type="text" class="form-control" name="address" placeholder="Nhập Địa Chỉ" value="{{$auth->user()->address}}">
            </div>
            <div class="form-group">
                <label for="">Phương thưc thanh toán</label>
                <select name="pay" id="inputPay" class="form-control" required>
                    <option value="">Chọn phương thưc thanh toán</option>
                    <option value="Tại cửa hàng">Tại cửa hàng</option>
                    <option value="Qua tài khoản ngân hàng">Qua tài khoản ngân hàng</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Phương thức vận chuyển</label>
                <select name="ship" id="inputPay" class="form-control" required>
                    <option value="">Chọn phương thưc vận chuyển</option>
                    <option value="Tại cửa hàng">Tại cửa hàng</option>
                    <option value="Chuyển qua bưu điện">Chuyển qua bưu điện</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Ghi chú thêm</label>
                <textarea name="note" class="form-control" rows="3"></textarea>
            </div>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <button type="submit" class="btn btn-primary">Đặt Hàng</button>
            <button type="submit" class="btn btn-primary"><a href="{{route('home.huong-hoadon')}}" title="">Hóa Đơn</a> </button>
        </form>
        @else
            <div class="jumbotron">
                <div class="container">
                    <h2>Giỏ hàng bị rỗng</h2>
                    <p>Vui lòng quay lại mua hàng ...</p>
                    <p>
                        <a class="btn btn-primary btn-lg" href="{{route('home')}}">Mua hàng</a>
                    </p>
                </div>
            </div>
        @endif
    @else
        <form action="{{route('home.huong-adduser')}}" method="POST" role="form" style="width: 300px;margin: 20px auto; ">
     <legend>Đăng Nhập</legend>
   @if(Session::has('info'))
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        {{ Session::get('info') }}
      </div>
    @endif
     <div class="form-group">
       <label for="">Tài Khoản</label>
       <input type="email" class="form-control" name="email" placeholder="Nhập Email">
       @if($errors->has('email'))
        <div class="help-block">
          <b style="color: red">Thông Tin Tài Khoản Sai</b>
          
        </div>
       @endif
     </div>
     <div class="form-group">
       <label for="">Mật Khẩu</label>
       <input type="password" class="form-control" name="password" placeholder="Nhập Mật Khẩu">
       @if($errors->has('password'))
       <div class="help-block">
         <b style="color: red">Thông Tin Tài Khoản Sai</b>
       </div>
       @endif
     </div>
     <input type="hidden" name="_token" value="{{csrf_token()}}">
     <button type="submit" class="btn btn-primary">Submit</button>
   </form>
    @endif
    </div>
    <div class="col-md-7">
        <table class="table table-hover" >
        <thead>
            <tr>
                <th width="10">STT</th>
                <th width="10">Ảnh</th>
                <th width="10">Tên Sản Phẩm</th>
                <th width="10">Giá</th>
                <th width="10">Số Lượng</th>
            </tr>
        </thead>
        <tbody>
            @php($n = 1)
            @if(count($carts)) @foreach($carts as $cart)
            <tr>
                <td>{{$n}}</td>
                <td  width="10">
                    <img src="{{url('uploads')}}/{{$cart['image']}} "  width="70" >
                </td>
                <td>{{$cart['name']}}</td>
                <td>{{number_format($cart['price'],0,'',',')}} đ</td>
                <td>
                    {{$cart['amount']}}
                </td>
            
            </tr>
            @php($n ++)
        @endforeach @endif
        </tbody>
        <div>
            <tr>
                <th colspan="3">Tổng số</th>
                <th>{{number_format($total,0,'',',')}} đ</th>
                <th colspan="2">{{$totalitem}} SL   </th>
            </tr>
        </div>
    </table>
    </div>
@stop()
