@extends('layouts.main-layout')

@section('content')

<div class="header-navigate clearfix mb15">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5">
				<ol class="breadcrumb breadcrumb-arrow" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
					<li><a href="{{route('home')}}" itemprop="url" target="_self">Trang chủ</a></li>
					
					
					<li><i class="fa fa-angle-right"></i></li>
					<li class="active"><span itemprop="title">{{$product->name}}</span></li>
					
				</ol>
			</div>
		</div>
	</div>
</div>

<section id="product" class="clearfix">
	<div class="container">
		<div class="row">
			<div id="surround" class="col-lg-6 col-md-6">
				<div class="row flexbox-product-slide">
							<ul>
								
								<li class="product-thumb">
									
										<img src="{{url('uploads').'/'.$product->image}}" class="img-responsive" width="400px">
									
								</li>
								
							</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 pd5 information-product">
				<div class="product-title">
					<h1>{{$product->name}}</h1>
				</div>
				<div class="clearfix product-sku-date">
					<!-- <span class="pull-left product-sku" id="pro_sku">Mã sản phẩm: 747356-106-1</span> -->
				</div>
				<div class="product-price" id="price-preview">
					
					<del>{{number_format($product['price'],0,'',',')}}</del><sup style="color: red;font-size: 20px">đ</sup>
					   -
					   <div class="product-pricesale-percent">
					<?php 
					$tax = ($product['price']-$product['sale_price']) / 100000;

					?>
					Giảm giá: {{$tax}} % </div>
					<span>{{number_format($product['sale_price'],0,'',',')}}</span><sup style="color: red;font-size: 20px">đ</sup>

					<!-- <script>
		$('.countdown_7680117').countdown('2017/8/24', function(event) {
			if ( event.type == 'update' ){
				$(this).show();
			}
		});
	</script> -->
					
					
				</div>
				
							
				<form id="add-item-form" action="{{route('home.search')}}" method="post" class="variants clearfix variant-style ">	
					<div class="select clearfix">
						
						
						<!-- <div class="selector-wrapper opt1">
							<label>Màu sắc</label>
							<ul class="data-opt1 clearfix style-variant-template">

							</ul>
						</div> -->
						
						
						<div class="selector-wrapper opt2">
							<label>Kích thước</label>
							<ul class="data-opt2 clearfix style-variant-template">
							</ul>
							<select name="size" id="">
								<option value="">35</option>
								<option value="">36</option>
								<option value="">37</option>
								<option value="">38</option>
								<option value="">39</option>
								<option value="">40</option>
							</select>


						
						
					</div>
						</div>
						
										
						
					<div class="select-wrapper clearfix price-contact">
						<!-- <label>Số lượng</label>
						<div class="input-group qty-control">
							<input type="button" value="-" onclick="Nobita.minusQuantity()" class="qty-btn">
							<input type="text" required id="quantity" pattern= "[0-9]" name="quantity" value="1" min="1" class="quantity-selector">
							<input type="button" value="+" onclick="Nobita.plusQuantity()" class="qty-btn">
						</div> -->
					</div>
					<div class="product-bottom">
				<br>
				<button type="button" style="width: 120px;height: 40px;"><b><a href="{{route('home.huong-addcart',['id'=>$product->id])}}">Thêm vào giỏ</a></b></button></div>
				</form>			
			
				
	


				<!-- <div class="info-socials-article clearfix">
					<div class="box-like-socials-article">
						<div class="fb-like" data-href="" data-layout="button_count" data-action="like">
						</div>
					</div> 
					<div class="box-like-socials-article">
						<div class="fb-share-button" data-href="" data-layout="button_count">
						</div>
					</div>
					<div class="box-like-socials-article">
						<div class="fb-send" data-href=""></div>
					</div>
				</div>
				 -->
				<div class="col-md-12">
					<h4>Viết bình luận...<span class="glyphicon glyphicon-pencil"></span></h4>
					@if($auth->check())
					<form action="{{route('frontend.comment')}}" method="POST" role="form">
						
						<div class="form-group">
							<textarea class="form-control" rows="3" name="comment"></textarea>
							<input type="hidden" name="id_sp" value="{{$product->id}}">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
						</div>
						<button type="submit" class="btn btn-primary">Gửi</button>
					</form>
					@else
						<div class="alert">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Vui lòng đăng nhập để bình luận</strong> <a href="{{route('home.huong-adduser')}}">Đăng nhập</a>
						</div>
					@endif
					<div class="comments">
						<h1>Các bình luận</h1>
					@foreach($comments as $com)
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="https://www.gravatar.com/avatar/fkhfkdhf.jpg?size=40" alt="Image">
							</a>
							<div class="media-body">
								<h4 class="media-heading">{{$com->khbl->full_name}}</h4>
								<p>{!!$com->comment!!}</p>
							</div>
						</div>
					@endforeach
					</div>
				</div>
				
				
			</div>

			<div class="col-lg-2 col-xs-12 pd-none-box-service mb15">
				<div class="box-service-product">
					<div class="header-box-service-product text-center">
						<div class="title">Sẽ có tại nhà bạn</div>
						<div class="content">từ 1-7 ngày làm việc</div>
					</div>
					<div class="content-box-service-product row">
						
						<div class="col-lg-12 col-sm-3 col-xs-12">
							<div class="border-service-product">
								<div class="flexbox-grid-default">
									<div class="flexbox-auto-45px flexbox-align-self-center">
										<img src="{{url('/')}}/public/images/icon-service-1.png" />
									</div>
									<div class="flexbox-content des-service-product">
										<div class="title">Giao hàng miễn phí</div>
										<div class="content">Sản phẩm trên 300,000đ</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="col-lg-12 col-sm-3 col-xs-12">
							<div class="border-service-product">
								<div class="flexbox-grid-default">
									<div class="flexbox-auto-45px flexbox-align-self-center">
										<img src="{{url('/')}}/public/images/icon-service-2.png" />
									</div>
									<div class="flexbox-content des-service-product">
										<div class="title">Đổi trả miễn phí</div>
										<div class="content">Đổi trả miễn phí 30 ngày</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="col-lg-12 col-sm-3 col-xs-12">
							<div class="border-service-product">
								<div class="flexbox-grid-default">
									<div class="flexbox-auto-45px flexbox-align-self-center">
										<img src="{{url('/')}}/public/images/icon-service-3.png" />
									</div>
									<div class="flexbox-content des-service-product">
										<div class="title">Thanh toán</div>
										<div class="content">Thanh toán khi nhận hàng</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="col-lg-12 col-sm-3 col-xs-12">
							<div class="border-service-product">
								<div class="flexbox-grid-default">
									<div class="flexbox-auto-45px flexbox-align-self-center">
										<img src="{{url('/')}}/public/images/icon-service-4.png" />
									</div>
									<div class="flexbox-content des-service-product">
										<div class="title">Hỗ trợ online</div>
										<div class="content">096.822.1530</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
@stop()