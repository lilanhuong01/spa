@extends('layouts.main-layout')

@section('content')
<form action="" method="POST" role="form">
	<legend>Đăng nhập</legend>
	@if(Session::has('info'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"></button>

		{{Session::get('info')}}
	</div>
	@endif
	<div class="form-group">
		<label for="">Tài khoản</label>
		<input type="email" class="form-control" name="email" placeholder="Nhập email">
		@if($errors->has('email'))
		<div class="help-block">
			<b style="color: red">Thông tin tài khoản sai</b>
		</div>
		@endif
	</div>
	<div class="form-group">
		<label for="">Mật khẩu</label>
		<input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu">
		@if($errors->has('password'))
		<div class="help-block">
			<b style="color: red">Thông tin tài khoản sai</b>
		</div>
		@endif
	</div>
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<button type="submit" class="btn btn-primary">Đăng nhập</button>
	

	
</form>

@stop()