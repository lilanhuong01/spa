<?php 
// quản lý tài khoản
	Route::get('auth-list',[
		'uses' => '\App\Http\Controllers\admin\AuthController@list',
		'as' => 'admin.auth-list'
	]);
	Route::get('auth-add',[
		'uses' => '\App\Http\Controllers\admin\AuthController@add',
		'as' => 'admin.auth-add'
	]);
	Route::get('auth-delete/{id}',[
		'uses' => '\App\Http\Controllers\admin\AuthController@authDelete',
		'as' => 'admin.auth-delete'
	]);
	Route::get('auth-logout',[
		'uses' => '\App\Http\Controllers\admin\AuthController@logout',
		'as' => 'admin.auth-logout'
	]);
	
	Route::post('auth-add',[
		'uses' => '\App\Http\Controllers\admin\AuthController@postAdd',
		'as' => 'admin.auth-add'
	]);
	Route::get('auth-edit/{id}',[
	'uses' => '\App\Http\Controllers\admin\AuthController@edit',
	'as' => 'admin.auth-edit'
	]);
	Route::post('auth-edit/{id}',[
		'uses' => '\App\Http\Controllers\admin\AuthController@update',
		'as' => 'admin.auth-edit'
]);
?>