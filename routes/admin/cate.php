<?php
Route::get('cate-list',[
		'uses' => '\App\Http\Controllers\admin\CateController@list',
		'as' => 'admin.cate-list'
	]);
	
	Route::get('cate-add',[
	'uses' => '\App\Http\Controllers\admin\CateController@add',
	'as' =>'admin.cate-add'
	]);
	Route::get('cate-delete/{id}',[
		'uses' => '\App\Http\Controllers\admin\CateController@cateDelete',
		'as' => 'admin.cate-delete'
	]);
	Route::post('cate-add',[
		'uses' => '\App\Http\Controllers\admin\CateController@postAdd',
		'as' =>'admin.cate-add'
	]);
	Route::get('cate-edit/{id}',[
	'uses' => '\App\Http\Controllers\admin\CateController@edit',
	'as' => 'admin.cate-edit'
	]);
	Route::post('cate-edit/{id}',[
		'uses' => '\App\Http\Controllers\admin\CateController@update',
		'as' => 'admin.cate-edit'
	]);
?>