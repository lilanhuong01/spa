<?php
//form
	Route::get('/huong-addkh',[
		'uses' => '\App\Http\Controllers\FrontendController@addkh',
		'as' => 'home.huong-addkh'
	]);
//end
	Route::post('/huong-addkh',[
		'uses' => '\App\Http\Controllers\FrontendController@userkh',
		'as' => 'home.huong-addkh'
	]);
	Route::get('/huong-adduser',[
		'uses' => '\App\Http\Controllers\FrontendController@adduser',
		'as' => 'home.huong-adduser'
	]);
	Route::post('/huong-adduser',[
		'uses' => '\App\Http\Controllers\FrontendController@loginuser',
		'as' => 'home.huong-adduser'
	]);
	Route::get('/huong-logout',[
		'uses' => '\App\Http\Controllers\FrontendController@logout',
		'as' => 'home.huong-logout'
	]);
	

	//dat hag
	Route::get('/huong-dathang',[
		'uses'=>'\App\Http\Controllers\FrontendController@dathang',
		'as'=>'home.dathang'
	]);
	
	Route::post('/huong-dathang',[
		'uses'=>'\App\Http\Controllers\FrontendController@adddathang',
		'as'=>'home.dathang'
	]);
	//hoa don
	Route::get('/huong-hoadon',[
		'uses'=>'\App\Http\Controllers\FrontendController@hoadon',
		'as'=>'home.huong-hoadon'
	]);

	//Khách hàng comment
	

	Route::post('/comment',[
		'uses'=>'\App\Http\Controllers\CustomerController@comment',
		'as'=>'frontend.comment'
	]);



?>