<?php 
// Khia báo các route quản lý sản phẩm

Route::get('product',[
	'uses' => '\App\Http\Controllers\admin\ProductController@index',
	'as' => 'admin.product'
]);

//sản phẩm mới
Route::get('product',[
	'uses' => '\App\Http\Controllers\admin\ProductController@newproduct',
	'as' => 'admin.newproduct'
]);


Route::get('product-create',[
	'uses' => '\App\Http\Controllers\admin\ProductController@create',
	'as' => 'admin.product-create'
]);
Route::post('product-create',[
	'uses' => '\App\Http\Controllers\admin\ProductController@store',
	'as' => 'admin.product-create'
]);
Route::get('product-delete/{id}',[
		'uses' => '\App\Http\Controllers\admin\ProductController@proDelete',
		'as' => 'admin.product-delete'
	]);

/**
* Thực hiện chỉnh sửa
* Có 2 phương thức là get để hiển thị form chỉnh sửa
* Phương thức post để thực hiện lấy dữ liệu và lưu vào DB
*/
Route::get('product-edit/{id}',[
	'uses' => '\App\Http\Controllers\admin\ProductController@edit',
	'as' => 'admin.product-edit'
]);
Route::post('product-edit/{id}',[
	'uses' => '\App\Http\Controllers\admin\ProductController@update',
	'as' => 'admin.product-edit'
]);
?>