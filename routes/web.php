<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//tao nhom group cho admin
//prefix:tien to...
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	Route::get('/',[
	'uses' => '\App\Http\Controllers\admin\HomeController@index',
	'as' =>'admin.index'
	]);
	require_once 'admin/auth.php';

	require_once 'admin/product.php';

	require_once 'admin/cate.php';



});


// frontend

Route::get('/login',[
	'uses' => '\App\Http\Controllers\admin\AuthController@index',
	'as' => 'admin.login'
]);
Route::post('/login',[
	'uses' => '\App\Http\Controllers\admin\AuthController@login',
	'as' => 'admin.login'
]);
Route::get('/',[
	'uses' => '\App\Http\Controllers\FrontendController@index',
	'as' =>'home'
]);


//Giỏ hàng
Route::get('/huong-addcart/{id}',[
	'uses' => '\App\Http\Controllers\FrontendController@addcart',
	'as' =>'home.huong-addcart'
]);
//Hiển thị giỏ hàng 
Route::get('/huong-cart',[
	'uses' => '\App\Http\Controllers\FrontendController@viewcart',
	'as' =>'home.huong-cart'
]);
//Sản phẩm mới
Route::get('/spmoi',[
	'uses' => '\App\Http\Controllers\FrontendController@spmoi',
	'as' =>'home.spmoi'
]);
Route::get('san-pham/{slag}',[
	'uses' => '\App\Http\Controllers\FrontendController@view_product',
	'as' =>'home.product-view'
]);
Route::get('gioi-thieu',[
	'uses' => '\App\Http\Controllers\FrontendController@gioithieu',
	'as' =>'home.gioithieu'
]);
Route::get('tintuc',[
	'uses' => '\App\Http\Controllers\FrontendController@tintuc',
	'as' =>'home.tintuc'
]);
Route::get('lienhe',[
	'uses' => '\App\Http\Controllers\FrontendController@lienhe',
	'as' =>'home.lienhe'
]);
Route::get('danh-muc/{id}',[
	'uses' => '\App\Http\Controllers\FrontendController@category',
	'as' =>'home.category'
]);

Route::get('/search',[
	'uses' => '\App\Http\Controllers\FrontendController@search',
	'as' => 'home.search'
]);
Route::get('/search',[
	'uses' => '\App\Http\Controllers\FrontendController@search',
	'as' => 'home.search'
]);





require_once 'admin/cart.php';
require_once 'admin/khachhang.php';