<?php $__env->startSection('content'); ?>
<form action="" method="POST" role="form" enctype="multipart/form-data">
	
	<div class="form-group">
		<label for="">Tên danh mục</label>
		<input type="text" class="form-control" name="name" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Danh mục cha</label>
		<select name="parent" id="inputParent" class="form-control" required="required">
			<option value="0">Không có</option>
			<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
	</div>
		<div class="form-group">
		<label for="">Trạng thái</label>
		<input type="text" class="form-control" name="status" placeholder="Input field">
	</div>
	<div class="form-group">
		<label for="">Ảnh danh mục</label>
		<input type="file" class="form-control" name="image" placeholder="Input field">
	</div>

	
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>