<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>BÁCH KHOA - APTECH</title>
	<link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="public/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/carousel.css">
	<link rel="stylesheet" type="text/css" href="public/css/fullPage.css">
	<link rel="stylesheet" type="text/css" href="public/css/slick.css">
	<link rel="stylesheet" type="text/css" href="public/css/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="public/css/landing-page.css">
	<link rel="stylesheet" type="text/css" href="public/css/landing-page-rps.css">
</head>
<body>
	<div id="wrapper">
		<div id="slide_1">
			<header id="header">
				<div class="container">
					<div class="row clearfix">
						<div class="col-sm-3 top-header-left">
							<a href="#" title="Bách khoa - Aptech">
								<img class="img-responsive" src="images/logo-bkap.png" alt="Hệ thống đào tạo CNTT Quốc Tế Bách Khoa - Aptech"/>
							</a>
						</div><!-- .top-header-left -->
						<div class="col-sm-6 text-right pull-right top-header-right">
							<h2>BACHKHOA-APTECH INTERNATIONAL IT EDUCATION SYSTEM</h2>
							<p>15 năm hoạt động - 30.000 HV đã tốt nghiệp - Chương trình đào tạo chuẩn quốc tế</p>
						</div><!-- .top-header-right -->
					</div><!-- .row -->
				</div>
			</header><!-- #header -->
			<div id="scrollTo_nav"></div>
			<nav id="main-menu">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav pull-left">
							<li data-anchor="slide_1"><a href="#slide_1" title="Trang chủ"><img src="images/home-icon.png" alt="bkap-home" /></a></li>
							<li data-anchor="slide_2"><a href="#slide_2" title="">GIỚI THIỆU</a></li>
							<li data-anchor="slide_3"><a href="#slide_3">MỤC TIÊU KHÓA HỌC</a></li>
							<li data-anchor="slide_4"><a href="#slide_4">NỘI DUNG ĐÀO TẠO</a></li>
							<li data-anchor="slide_5"><a href="#slide_5">CAM KẾT</a></li>
							<li data-anchor="slide_6"><a href="#slide_6">VỀ CHÚNG TÔI</a></li>
							<li data-anchor="slide_8"><a href="#slide_8">ĐĂNG KÝ</a></li>
						</ul><!-- .nav .navbar-nav -->

						<div class="pull-right nav-hotline hidden-sm">
							<b>Hotline: 0968.27.6996</b>
						</div>
					</div>
				</div>
			</nav><!-- #main-menu -->

			<section class="banner">
				<div class="banner-bg-color">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 banner-left">
								<h1>KHÓA HỌC</h1>
								<h2>LẬP TRÌNH WEB VỚI PHP & MySQL</h2>
								<p>"Tự tay dụng ngay website riêng chạy online"</p>
							</div><!-- .banner-left -->
							<div class="col-sm-5 banner-right pull-right">
								<form>
									<div class="form-horizontal form-banner">
										<h2>ĐĂNG KÝ NGAY</h2>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Họ và tên">
											</div>
										</div>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Email">
											</div>
										</div>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Số điện thoại">
											</div>
										</div>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Địa chỉ">
											</div>
										</div>
										<div class="form-group">
											<div class="text-center">
												<button class="btn btn-warning" type="submit">GỬI</button>
											</div>
										</div>
									</div><!-- .form-horizontal -->
								</form>
							</div><!-- .banner-right -->
						</div><!-- .row -->
					</div>
				</div>	
			</section><!-- .banner -->
		</div>
		<div id="slide_2">
			<section id="box-intro" class="box-intro">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 video-box-intro">
							<div class="iframe-video">
								<iframe src="https://www.youtube.com/embed/X-q_XhP9JaQ" frameborder="0" gesture="media" allowfullscreen style="width: 100%"></iframe>
							</div>
						</div><!-- .video-intro -->
						<div class="col-sm-6 content-box-intro">
							<h2 class="title-content-intro">GIỚI THIỆU</h2>
							<div class="content-intro">
								<div class="item-content-intro">
									<h3>1. PHP là gì?</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur expedita cum alias sint illo odit voluptatibus sequi, dolorum reprehenderit assumenda adipisci laudantium voluptatum non quo nesciunt eum molestias officia, cupiditate.</p>
								</div>
								<div class="item-content-intro">
									<h3>1. PHP là gì?</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur expedita cum alias sint illo odit voluptatibus sequi, dolorum reprehenderit assumenda adipisci laudantium voluptatum non quo nesciunt eum molestias officia, cupiditate.</p>
								</div>
							</div>
						</div><!-- .content-intro -->
					</div><!-- .row -->
				</div>
			</section><!-- .box-intro -->
		</div>
		<?php echo $__env->yieldContent('content'); ?>
		<!-- slide_8 -->
		<div id="slide_9">
			<footer id="footer">
				<div class="bg-footer">
					<div class="container">
						<div class="content-footer">
							<div class="row">
								<div class="col-sm-4 item-content-footer">
									<div class="item-ctf-title">
										<h3>VỀ BÁCH KHOA - APTECH</h3>
									</div>
									<div class="item-ctf-content">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero molestiae fugiat aliquid eaque asperiores, dicta, inventore recusandae eum fuga optio qui neque libero quam minima numquam, officia fugit, illum. Ex.</p>
									</div>
								</div>
								<div class="col-sm-4 item-content-footer">
									<div class="item-ctf-title">
										<h3>LIÊN HỆ</h3>
									</div>
									<div class="item-ctf-content">
										<p><i class="fa fa-map-marker"></i>Tòa nhà HTC, 236B & 238 Hoàng Quốc Việt, Từ Liêm, Hà Nội.</p>
										<p><i class="fa fa-map-marker"></i>0968.27.6996 / 024 3755 4010</p>
										<p><i class="fa fa-map-marker"></i>tuyensinh@bachkhoa-aptech.edu.vn<br>
										bachkhoa-aptech.edu.vn</p>
										<div class="item-ctf-bot">
											<h4>THEO DÕI BÁCH KHOA - APTECH</h4>
											<ul>
												<li><a href=""><img class="img-responsive" src="images/icon-fb.png" alt=""></a></li>
												<li><a href=""><img class="img-responsive" src="images/icon-yt.png" alt=""></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 item-content-footer">
									<div class="item-ctf-title">
										<h3>BẢN ĐỒ</h3>
									</div>
									<div class="map">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1031.6996424055728!2d105.78288637461966!3d21.046437696911585!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab3b4220c2bd%3A0x1c9e359e2a4f618c!2sB%C3%A1ch+Khoa+Aptech!5e0!3m2!1svi!2sus!4v1509699609675" frameborder="0" style="border:0" allowfullscreen></iframe>
										<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.6455042745147!2d105.78425261418018!3d21.046865692537434!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab326aa9bfef%3A0xd60313e072fc5ac4!2zMjM2IEhvw6BuZyBRdeG7kWMgVmnhu4d0LCBD4buVIE5odeG6vyAxLCBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1509508754826" frameborder="0" style="border:0" allowfullscreen></iframe> -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="bot-footer">
						<p>Bản quyền thuộc về Bách Khoa - Aptech</p>
					</div>
				</div>
			</footer><!-- #footer -->
		</div>
		<!-- slide_9 -->
	</div><!-- #wrapper -->
	
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/owl.carousel.js"></script>
	<script type="text/javascript" src="js/jquery.carousel-1.1.min.js"></script>
	<script type="text/javascript" src="js/fullPage.js"></script>
	<script type="text/javascript" src="js/slick.js"></script>
	<script type="text/javascript" src="js/jquery.bkap.js"></script>
</body>
</html>