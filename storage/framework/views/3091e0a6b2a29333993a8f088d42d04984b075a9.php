<?php $__env->startSection('content'); ?>
<form action="" method="POST" role="form">
	<legend>Đăng nhập</legend>
	<?php if(Session::has('info')): ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"></button>

		<?php echo e(Session::get('info')); ?>

	</div>
	<?php endif; ?>
	<div class="form-group">
		<label for="">Tài khoản</label>
		<input type="email" class="form-control" name="email" placeholder="Nhập email">
		<?php if($errors->has('email')): ?>
		<div class="help-block">
			<b style="color: red">Thông tin tài khoản sai</b>
		</div>
		<?php endif; ?>
	</div>
	<div class="form-group">
		<label for="">Mật khẩu</label>
		<input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu">
		<?php if($errors->has('password')): ?>
		<div class="help-block">
			<b style="color: red">Thông tin tài khoản sai</b>
		</div>
		<?php endif; ?>
	</div>
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	<button type="submit" class="btn btn-primary">Đăng nhập</button>
	

	
</form>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>