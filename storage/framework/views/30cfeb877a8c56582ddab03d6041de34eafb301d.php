<?php $__env->startSection('content'); ?>

		<div class="container mb15 group-index" id="group-sale-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="title-block">
                <div class="wrap-content">
                    
                    <h2 class="title-group">Black Weekend</h2>
                    
                    <div class="title-group-note">Chương trình sẽ kết thúc sau</div>
                </div>
            </div>
        </div>
        
        
        <div class="col-xs-12">
            <div class="box-count-number flexbox-grid-default">
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-day">23</span>
                        <div class="clearfix"></div>
                        <p class="times-dots">ngày</p>
                    </div>
                </div>
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-hour">23</span>
                        <div class="clearfix"></div>
                        <p class="times-dots">giờ</p>
                    </div>
                </div>
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-minute">59</span>
                        <div class="clearfix"></div>
                        <span class="times-dots">phút</span>
                    </div>
                </div>
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-section">59</span>
                        <div class="clearfix"></div>
                        <span class="times-dots">giây</span>
                    </div>
                </div>
            </div>
            <script>
                var time_deal = '2017/12/01'; 
                if ( time_deal != '' ) {
                    $('.box-count-number').countdown(time_deal, function(event) {
                        if ( event.type == 'update' ){
                            if ( event.offset.totalDays.toString().length == 1 ) {
                                jQuery('#count-day').html('0' + event.offset.totalDays);
                            } else {
                                jQuery('#count-day').html(event.offset.totalDays);
                            }       
                            if ( event.offset.hours.toString().length == 1 ) {
                                jQuery('#count-hour').html('0' + event.offset.hours);
                            } else {
                                jQuery('#count-hour').html(event.offset.hours);
                            }                                   
                            if ( event.offset.minutes.toString().length == 1 ) {
                                jQuery('#count-minute').html('0' + event.offset.minutes);
                            } else {
                                jQuery('#count-minute').html(event.offset.minutes);
                            }
                            if ( event.offset.seconds.toString().length == 1 ) {
                                jQuery('#count-section').html('0' + event.offset.seconds);
                            } else {
                                jQuery('#count-section').html(event.offset.seconds);
                            }
                        }
                    });
                }
            </script>
        </div>
        
        
    </div>

    <div class="row box-product-lists clear-item">
<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-wrapper product-resize mb30 ">
    <div class="product-information">
        <div class="product-detail clearfix">
            <div class="product-image image-resize">
                <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" title="Nike LunarTempo 2">
                    
                    <picture>
                        <source media="(max-width: 991px)" srcset="<?php echo e(url('uploads')); ?>/<?php echo e($product->image); ?>)">
                        <source media="(min-width: 992px)" srcset="<?php echo e(url('uploads')); ?>/<?php echo e($product->image); ?>">
                        <img src="<?php echo e(url('uploads')); ?>/<?php echo e($product->image); ?>" width="250px " alt="Nike LunarTempo 2" />
                    </picture>
                    
                </a>
                
                <div class="product-icon-new countdown_7680119" style="display: none;">
                    <svg class="svg-next-icon svg-next-icon-size-36" style="fill:#d80027">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-new-product"></use>
                    </svg>
                </div>
                <div class="box-position-quickview">
                    <div class="wrap-btn-quickview">
                        <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" class="quickview" data-handle="/nike-lunartempo-2" title="Xem nhanh">Xem nhanh</a>
                    </div>
                </div>
            </div>
            <div class="product-info">
                <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" title="Nike LunarTempo 2">
                    <h3 class="product-title" style="text-align: center; color: red;"><?php echo e($product->name); ?></h3>
                </a>
                <p class="product-box-price clearfix flexbox-grid-default">
                    <?php if($product->sale_price>0): ?>
                    <del class="price-new flexbox-content text-left"><?php echo e(number_format($product['price'],0,'',',')); ?><sup>đ</sup></del>
                    <span class="price-new flexbox-content text-left"><?php echo e(number_format($product['sale_price'],0,'',',')); ?><sup>đ</sup></span>
                
                    <?php else: ?>
                    <span class="price-new flexbox-content text-left"><?php echo e(number_format($product['price'],0,'',',')); ?></span>
                <?php endif; ?>
                    
                </p>
                <button type="button" style="border-radius: 5px; background: red;"> <b><a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" style="color: white; text-align: center;">Đặt Mua</a></b></button>
                
            </div>
        </div>
    </div>
    
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<div class="pro-pagination clearfix">
    <?php echo e($products->links()); ?>

</div>
<?php $__env->stopSection(); ?>    



<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>