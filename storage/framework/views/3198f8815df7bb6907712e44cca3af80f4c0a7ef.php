<?php $__env->startSection('content'); ?>
<form action="" method="POST" role="form">
	
	<div class="form-group">
		<label for="">Fullname</label>
		<input type="text" class="form-control" name="full_name" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Username</label>
		<input type="text" class="form-control" name="username" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Email</label>
		<input type="text" class="form-control" name="email" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Password</label>
		<input type="text" class="form-control" name="password" placeholder="Input field">
	</div>

	
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>