  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Laravel Version</b> 5.4
    </div>
    <strong>Copyright &copy; <?php echo e(date('Y')); ?> <a href="https://bachkhoa-aptech.edu.vn" target="_blank">LeoShop.vn</a>.</strong> All rights reserved.
  </footer>

</div>
<!-- ./wrapper -->

<script src="<?php echo e(url('/')); ?>/public/admin/js/jquery.min.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/js/jquery-ui.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/js/bootstrap.min.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/js/adminlte.min.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/js/dashboard.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/tinymce/tinymce.min.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/tinymce/config.js"></script>
<script src="<?php echo e(url('/')); ?>/public/admin/js/function.js"></script>
<?php echo $__env->yieldContent('footer_script'); ?>
<script type="text/javascript">
   $('tbody').sortable({
    start: function(event, ui) {
        var start_pos = ui.item.index();
        // alert(start_pos);
        ui.item.data('start_pos', start_pos);
    },
    change: function(event, ui) {
          var start_pos = ui.item.data('start_pos');
          var index = ui.placeholder.index();
          // alert(index);
      },
      update: function(event, ui) {
         var start_pos = ui.item.data('start_pos');
          var index = ui.placeholder.index();
      }
  });
</script>
<div class="modal fade" id="modal-file">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Quản lý ảnh</h4>
      </div>
      <div class="modal-body">
       
      </div>
    </div>
  </div>
</div>
</body>
</html>
