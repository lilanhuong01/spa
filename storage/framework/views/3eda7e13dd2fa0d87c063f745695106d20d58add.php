<?php $__env->startSection('content'); ?>

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Username</th>
			<th>Email</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td><?php echo e($model->id); ?></td>
			<td><?php echo e($model->username); ?></td>
			<td><?php echo e($model->email); ?></td>
			<td>
				<a href="<?php echo e(route('admin.auth-edit',['id'=>$model->id])); ?>" title="Xem" class="label label-success">Sửa</a>
				<?php if(Auth::user()->id == $model->id): ?>

				<?php else: ?>
				<a href="<?php echo e(route('admin.auth-delete',['id'=>$model->id])); ?>" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
				<?php endif; ?>
				
			</td>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title','Danh sách người dùng'); ?>
<?php $__env->startSection('sub-title','các tài khoản quản trị'); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>