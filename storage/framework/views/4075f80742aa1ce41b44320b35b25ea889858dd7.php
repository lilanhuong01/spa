<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo url('/') ?>/public/admin/images/huong.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><font color='yellow'><?php echo e(Auth::user()->full_name); ?></font></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
   
   <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li><a href="<?php echo e(route('admin.index')); ?>"><i class="fa fa-circle-o"></i> Trang chủ</a></li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-user"></i> <span>Tài khoản</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="active"><a href="<?php echo e(route('admin.auth-list')); ?>"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        <li class="active"><a href="<?php echo e(route('admin.auth-add')); ?>"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
        <li><a href=""><i class="fa fa-circle-o"></i> Thông tin cá nhân</a></li>
        <li><a href=""><i class="fa fa-circle-o"></i> Thay đổi mật khẩu</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-file-word-o"></i> <span>Quản lý danh mục</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="active"><a href="<?php echo e(route('admin.cate-list')); ?>"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        <li class="active"><a href="<?php echo e(route('admin.cate-add')); ?>"><i class="fa fa-circle-o"></i> Thêm mới</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-hand-o-right"></i> <span>Quản lý sản phẩm</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="active"><a href="<?php echo e(route('admin.product')); ?>"><i class="fa fa-circle-o"></i> Danh sách sản phẩm</a></li>
        <li class="active"><a href="<?php echo e(route('admin.product-create')); ?>"><i class="fa fa-circle-o"></i> Thêm mới sản phẩm</a></li>
      </ul>
    </li>
  </ul>
</section>
<!-- /.sidebar -->
</aside>