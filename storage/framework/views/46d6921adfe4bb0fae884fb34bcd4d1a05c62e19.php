<?php $__env->startSection('title','Quản lý sản phẩm'); ?>
<?php $__env->startSection('content'); ?>
<form action="" method="post" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-9">
			<div class="form-group">
				<label for="">Tên sản phẩm</label>
				<input type="text" class="form-control" name="name" placeholder="Tên sản phẩm..." />
			</div>
			<div class="form-group">
				<label for="">Đường dẫn tĩnh</label>
				<input type="text" class="form-control" name="slag" placeholder="Đường dẫn tĩnh..." />
			</div>
			<div class="form-group">
				<label for="">Ảnh sản phẩm</label>
				<input type="file" name="image"  />
			</div>
			<div class="form-group">
				<label for="">Mô tả sản phẩm</label>
				<textarea name="descriptions" class="form-control" rows="3" placeholder="Mô tả sản phẩm"></textarea>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="">Danh mục</label>
				<select name="catId"  class="form-control" required>
					<option value="">Chọn danh mục</option>
				<?php if($cats->count()): ?> <?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label for="">Giá</label>
				<input type="text" class="form-control" name="price" placeholder="Tên sản phẩm..." />
			</div>
			<div class="form-group">
				<label for="">Giá sale</label>
				<input type="text" class="form-control" name="sale_price" placeholder="Tên sản phẩm..." />
			</div>
			<div class="form-group">
				<label for="">Trạng thái</label>
				<select name="status"  class="form-control">
					<option value="0">Ẩn</option>
					<option value="1">Hiển thị</option>
				</select>
			</div>
		</div>
	</div>
	
	
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>