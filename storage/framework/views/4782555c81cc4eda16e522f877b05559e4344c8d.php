<?php $__env->startSection('content'); ?>

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Parent</th>
			<th>Status</th>
			<th>Image</th>
		</tr>
	</thead>
	<tbody>
	<?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td><?php echo e($model->id); ?></td>
			<td><?php echo e($model->name); ?></td>
			<td><?php echo e($model->parent); ?></td>
			<td><?php echo e($model->status); ?></td>
			<td><?php echo e($model->image); ?></td>
			<td>
				<a href="<?php echo e(route('admin.cate-edit',['id'=>$model->id])); ?>" title="Sửa" class="label label-success">Sửa</a>
				<a href="<?php echo e(route('admin.cate-delete',['id'=>$model->id])); ?>" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
			</td>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title','Danh sách danh mục'); ?>
<?php $__env->startSection('sub-title','các danh mục sản phẩm'); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>