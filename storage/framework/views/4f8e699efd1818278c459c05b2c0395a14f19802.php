<?php $__env->startSection('content'); ?>

<div style="margin: 20px auto; width: 400px">
	<form action="" method="POST" role="form">
		<legend>Form khách hàng đăng nhập</legend>

			<div class="form-group">
				<label for="">Fullname</label>
				<input type="text" class="form-control" name="full_name" placeholder="Input field">
			</div>
			<div class="form-group">
				<label for="">Name</label>
				<input type="text" class="form-control" name="name" placeholder="Input field">
			</div>
			<div class="form-group">
				<label for="">Email</label>
				<input type="text" class="form-control" name="email" placeholder="Input field">
			</div>
			<div class="form-group">
				<label for="">Password</label>
				<input type="text" class="form-control" name="password" placeholder="Input field">
			</div>
		
			<div class="form-group">
				<label for="">Phone</label>
				<input type="text" class="form-control" name="phone" placeholder="Input field">
			</div>
			<div class="form-group">
				<label for="">Address</label>
				<input type="text" class="form-control" name="address" placeholder="Input field">
			</div>
		
		
			
			<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
			<button type="submit" class="btn btn-primary">Đăng ký</button>
		
	</form>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>