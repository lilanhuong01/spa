<?php $__env->startSection('content'); ?>
<form action="" method="POST" role="form">
	
	<div class="form-group">
		<label for="">Tên danh mục</label>
		<input type="text" class="form-control" name="id_dm" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Tên sản phẩm</label>
		<input type="text" class="form-control" name="name_sp" placeholder="Input field">
	</div>
		<div class="form-group">
		<label for="">Ảnh sản phẩm</label>
		<input type="text" class="form-control" name="image" placeholder="Input field">
	</div>
	</div>
		<div class="form-group">
		<label for="">Giá sản phẩm</label>
		<input type="float" class="form-control" name="gia" placeholder="Input field">
	</div>
	</div>
		<div class="form-group">
		<label for="">Giá giảm</label>
		<input type="float" class="form-control" name="giagiam" placeholder="Input field">
	</div>
	</div>
		<div class="form-group">
		<label for="">Bảo hành</label>
		<input type="text" class="form-control" name="baohanh" placeholder="Input field">
	</div>
		</div>
		<div class="form-group">
		<label for="">Tình trạng</label>
		<input type="text" class="form-control" name="tinhtrang" placeholder="Input field">
	</div>
		</div>
		<div class="form-group">
		<label for="">Khuyến mại</label>
		<input type="text" class="form-control" name="khuyenmai" placeholder="Input field">
	</div>
		</div>
		<div class="form-group">
		<label for="">Đặc biệt</label>
		<input type="number" class="form-control" name="dacbiet" placeholder="Input field">
	</div>
		</div>
		<div class="form-group">
		<label for="">Chi tiết sản phẩm</label>
		<input type="text" class="form-control" name="chitietsp" placeholder="Input field">
	</div>
	
	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>