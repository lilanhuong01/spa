
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MyShoes | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo url('/') ?>/public/admin/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo url('/') ?>/public/admin/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo url('/') ?>/public/admin/css/AdminLTE.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Đăng nhập vào hệ thống</p>
    <?php if(Session::has('error')): ?>
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo e(Session::get('error')); ?>

      </div>
    <?php endif; ?>
    <?php echo $__env->yieldContent('content'); ?>
      <div class="clearfix"></div>
    <div class="text-left">
       <a href="#">Quên mật khẩu</a>
    </div>

  </div>
  <!-- /.login-box-body -->

</div>
<!-- jQuery 3 -->
<script src="<?php echo url('/') ?>/public/admin/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo url('/') ?>/public/admin/js/bootstrap.min.js"></script>

</body>
</html>
