<?php $__env->startSection('content'); ?>

		<div class="container mb15 group-index" id="group-sale-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="title-block">
                <div class="wrap-content">
                    
                    <h2 class="title-group">Black Weekend</h2>
                    
                    <div class="title-group-note">Chương trình sẽ kết thúc sau</div>
                </div>
            </div>
        </div>
        
        
        <div class="col-xs-12">
            <div class="box-count-number flexbox-grid-default">
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-day">23</span>
                        <div class="clearfix"></div>
                        <p class="times-dots">ngày</p>
                    </div>
                </div>
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-hour">23</span>
                        <div class="clearfix"></div>
                        <p class="times-dots">giờ</p>
                    </div>
                </div>
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-minute">59</span>
                        <div class="clearfix"></div>
                        <span class="times-dots">phút</span>
                    </div>
                </div>
                <div class="box-time flexbox-content">
                    <div class="text-center">
                        <span class="number-count-btn" id="count-section">59</span>
                        <div class="clearfix"></div>
                        <span class="times-dots">giây</span>
                    </div>
                </div>
            </div>
            <script>
                var time_deal = '2017/12/01'; 
                if ( time_deal != '' ) {
                    $('.box-count-number').countdown(time_deal, function(event) {
                        if ( event.type == 'update' ){
                            if ( event.offset.totalDays.toString().length == 1 ) {
                                jQuery('#count-day').html('0' + event.offset.totalDays);
                            } else {
                                jQuery('#count-day').html(event.offset.totalDays);
                            }       
                            if ( event.offset.hours.toString().length == 1 ) {
                                jQuery('#count-hour').html('0' + event.offset.hours);
                            } else {
                                jQuery('#count-hour').html(event.offset.hours);
                            }                                   
                            if ( event.offset.minutes.toString().length == 1 ) {
                                jQuery('#count-minute').html('0' + event.offset.minutes);
                            } else {
                                jQuery('#count-minute').html(event.offset.minutes);
                            }
                            if ( event.offset.seconds.toString().length == 1 ) {
                                jQuery('#count-section').html('0' + event.offset.seconds);
                            } else {
                                jQuery('#count-section').html(event.offset.seconds);
                            }
                        }
                    });
                }
            </script>
        </div>
        
        
    </div>

    <div class="row box-product-lists clear-item">
<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-wrapper product-resize mb30 ">
    <div class="product-information">
        <div class="product-detail clearfix">
            <div class="product-image image-resize">
                <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" title="Nike LunarTempo 2">
                    
                    <picture>
                        <source media="(max-width: 991px)" srcset="uploads/<?php echo e($product->image); ?>">
                        <source media="(min-width: 992px)" srcset="uploads/<?php echo e($product->image); ?>">
                        <img src="uploads/<?php echo e($product->image); ?>" alt="Nike LunarTempo 2" />
                    </picture>
                    
                </a>

                
                <div class="product-icon-new countdown_7680119" style="display: none;">
                    <svg class="svg-next-icon svg-next-icon-size-36" style="fill:#d80027">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-new-product"></use>
                    </svg>
                </div>
                <div class="box-position-quickview">
                    <div class="wrap-btn-quickview">
                        <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" class="quickview" data-handle="/nike-lunartempo-2" title="Xem nhanh">Xem nhanh</a>
                    </div>
                </div>
            </div>
            <div class="product-info">
                <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" title="Nike LunarTempo 2">
                    <h3 class="product-title" style="text-align: center; color: red;"><?php echo e($product->name); ?></h3>
                </a>
                <!-- <div class='price'><?php echo e($product->price); ?><sup> đ</sup></div>
                <div class='price_km'><?php echo e($product->sale_price); ?><sup> đ</sup></div> -->
                <p class="product-box-price clearfix flexbox-grid-default">
                    <div>
                    <?php if($product->sale_price>0): ?>
                    <del><?php echo e(number_format($product['price'],0,'',',')); ?></del>VNĐ
                       -
                       
                    <?php 
                    $tax = round((100-($product['sale_price']*100)/($product['price'])));

                    ?>
                    <p style="color:blue">Giảm giá: <?php echo e($tax); ?> % </p>
                    <span style="color: red;font-size: 20px"><?php echo e(number_format($product['sale_price'],0,'',',')); ?>VNĐ</span>

                    <script>
        $('.countdown_7680117').countdown('2017/8/24', function(event) {
            if ( event.type == 'update' ){
                $(this).show();
            }
        });
    </script>
                <?php endif; ?>
                    </div>
                </p>
                <!--  --><!-- <a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>">Xem</a>
                <a href="<?php echo e(route('home.huong-addcart',['id'=>$product->id])); ?>">Giỏ hàng</a>
                                 -->
                <button type="button" style="border-radius: 5px; background: red;float: left;"> <b><a href="<?php echo e(route('home.product-view',['slag'=>$product->slag])); ?>" style="color: white;">Xem Hàng</a></b></button>
                 <button type="button" style="border-radius: 5px; background: red; float: left; margin-left: 10px"> <b><a href="<?php echo e(route('home.huong-addcart',['slag'=>$product->slag])); ?>" style="color: white;">Mua ngay</a></b></button>
            </div>
        </div>
    </div>
    <script>
        $('.countdown_7680115').countdown('2017/8/24', function(event) {
            if ( event.type == 'update' ){
                $(this).show();
            }
        });
    </script>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<div class="pro-pagination clearfix">
    <?php echo e($products->links()); ?>

</div>
<div class="container mb15 group-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="title-block">
                <div class="wrap-content">
                    <h2 class="title-group">Danh mục ưu thích</h2>
                    <div class="title-group-note">Danh mục được nhiều khách hàng yêu thích</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
       
        
                 <?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                     <!-- <a href="<?php echo e(route('home.category',['id'=> $cat->id])); ?>" title="Giày yêu thích"></a> -->
                    <?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mb30 box-banner-index">
             
                                <li class="current">
                                    <a href="<?php echo e(route('home.category',['id'=> $cat->id])); ?>" title="Giày yêu thích"> <img src="<?php echo e(url('uploads').'/'.$c->image); ?>" class="img-responsive" >
                                    </a>
                                   
                                </li>
                           </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            
    </div>
        </div>
       
    </div>
</div>







<!-- Sản phẩm bán chạy -->
<div class="container mb15 group-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="title-block">
                <div class="wrap-content">
                    
                    <h2 class="title-group">Sản phẩm bán chạy</h2>
                    
                    <div class="title-group-note">Hàng luôn được cập nhật thường xuyên</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row box-product-lists clear-item">

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-wrapper product-resize mb30 ">
    <div class="product-information">
        <div class="product-detail clearfix">
            <div class="product-image image-resize">
                <a href="/nike-train-speed-4" title="Nike Train Speed 4">
                    
                    <picture>
                        <source media="(max-width: 991px)" srcset="//bizweb.dktcdn.net/thumb/medium/100/238/538/products/dotrainspeed4trainingshoe1.jpg?v=1500949657360">
                        <source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/thumb/large/100/238/538/products/dotrainspeed4trainingshoe1.jpg?v=1500949657360">
                        <img src="//bizweb.dktcdn.net/thumb/large/100/238/538/products/dotrainspeed4trainingshoe1.jpg?v=1500949657360" alt="Nike Train Speed 4" />
                    </picture>
                    
                </a>
                
                <div class="product-icon-new countdown_7680127" style="display: none;">
                    <svg class="svg-next-icon svg-next-icon-size-36" style="fill:#d80027">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-new-product"></use>
                    </svg>
                </div>
                <div class="box-position-quickview">
                    <div class="wrap-btn-quickview">
                        <a href="javascript:void(0);" class="quickview" data-handle="/nike-train-speed-4" title="Xem nhanh">Xem nhanh</a>
                    </div>
                </div>
            </div>
            <div class="product-info">
                <a href="/nike-train-speed-4" title="Nike Train Speed 4">
                    <h3 class="product-title">Nike Train Speed 4</h3>
                </a>
                <p class="product-box-price clearfix flexbox-grid-default">
                    
                    <span class="price-new flexbox-content text-left">3.000.000₫</span>
                    
                    
                </p>
            </div>
        </div>
    </div>
    <script>
        $('.countdown_7680127').countdown('2017/8/24', function(event) {
            if ( event.type == 'update' ){
                $(this).show();
            }
        });
    </script>
</div>
        
        
        
        
        




<?php $__env->stopSection(); ?>    
<!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/59d22dfbc28eca75e4623942/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();
            </script>
            <!--End of Tawk.to Script-->



<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>