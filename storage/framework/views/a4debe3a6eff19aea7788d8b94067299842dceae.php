<?php $__env->startSection('content'); ?>

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID danh mục</th>
			<th>Tên sản phẩm</th>
			<th>Ảnh sản phẩm</th>
			<th>Giá sản phẩm</th>
			<th>Giá giảm</th>
			<th>Bảo hành</th>
			<th>Tình trạng</th>
			<th>Khuyến mại</th>
			<th>Đặc biệt</th>
			<th>Chi tiết sản phẩm</th>
		</tr>
	</thead>
	<tbody>
	<?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td><?php echo e($model->id_dm); ?></td>
			<td><?php echo e($model->name_sp); ?></td>
			<td><?php echo e($model->anhsp); ?></td>
			<td><?php echo e($model->giasp); ?></td>
			<td><?php echo e($model->giagiam); ?></td>
			<td><?php echo e($model->baohanh); ?></td>
			<td><?php echo e($model->tinhtrang); ?></td>
			<td><?php echo e($model->khuyenmai); ?></td>
			<td><?php echo e($model->dacbiet); ?></td>
			<td><?php echo e($model->chitietsp); ?></td>
			<td>
				<a href="#" title="Xem" class="label label-success">Sửa</a>
				<a href="<?php echo e(route('admin.cate-delete',['id'=>$model->id])); ?>" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
			</td>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title','Danh sách sản phẩm'); ?>
<?php $__env->startSection('sub-title','các sản phẩm'); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>