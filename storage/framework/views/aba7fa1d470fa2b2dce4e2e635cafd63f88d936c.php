<?php $__env->startSection('content'); ?>
	<div  >
		<form action="" method="POST" role="form" style="width: 350px;margin: 20px auto; ">
			<legend>Hóa Đơn</legend>
		
			<div class="form-group">
				<label for="">Tên Khách Hàng</label>
				<input type="text" class="form-control" name="name" placeholder="Tên Khách Hàng">
			</div>
			<div class="form-group">
				<label for="">Địa Chỉ</label>
				<input type="text" class="form-control"  name="address" placeholder="Địa Chỉ">
			</div>
			<div class="form-group">
				<label for="">Tổng Tiền</label>
				<input type="text" class="form-control" name="sum_price" placeholder="Tổng Tiền">
			</div>
			<div class="form-group">
				<label for="">Phương Thức Thanh Toán</label>
				<input type="text" class="form-control" name="pay" placeholder="Phương Thức Thanh Toán">
			</div>
			<div class="form-group">
				<label for="">Phương Thức Vận Chuyển</label>
				<input type="text" class="form-control" name="ship" placeholder="Phương Thức Vận Chuyển">
			</div>
			<div class="form-group">
				<label for="">Ghi Chú</label>
				<input type="text" class="form-control" name="note" placeholder="Ghi Chú">
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>