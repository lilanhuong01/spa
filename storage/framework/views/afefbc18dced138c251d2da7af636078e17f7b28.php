<!DOCTYPE html>

<html lang="vi">
    <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name='revisit-after' content='1 days' />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' /><![endif]-->
        <title>MyShoes </title>
        
        <meta name="keywords" content="Giày thể thao, giày nike, giày adidas, giày công sổ, giày phong cách" />
        
        
        <meta name="description" content="MyShoes là thương hiệu giày thời trang hàng đầu Việt Nam. Bạn sẽ dễ dàng chọn cho mình một sản phẩm giày hiệu chất lượng và cực kỳ phong cách." />
        
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=0' name='viewport' />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="MyShoes" />
          
        <meta property="og:image" content="public/images/logo.png" />
        <meta property="og:image:secure_url" content="images/logo.png" />
        <meta property="og:description" content="Rossy Store l&#224; thương hiệu gi&#224;y thời trang h&#224;ng đầu Việt Nam. Bạn sẽ dễ d&#224;ng chọn cho m&#236;nh một sản phẩm gi&#224;y hiệu chất lượng v&#224; cực kỳ phong c&#225;ch." />

       
        <meta property="og:site_name" content="Rossy Store" />
        <script src='<?php echo e(url("/")); ?>/public/js/jquery-1.12.4.min.js' type='text/javascript'></script>
        <script src='<?php echo e(url("/")); ?>/public/js/option-selectors.js' type='text/javascript'></script>
        <script src='<?php echo e(url("/")); ?>/public/js/api.jquery.js' type='text/javascript'></script>
        <script src="<?php echo e(url('/')); ?>/public/js/jquery.cookie.js"></script>
        <script src="<?php echo e(url('/')); ?>/public/js/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo e(url('/')); ?>/public/js/bootstrap.min.js"></script>
        <script src="<?php echo e(url('/')); ?>/public/js/owl.carousel_v2.0.0-beta.2.4.js"></script>
        <script src="<?php echo e(url('/')); ?>/public/js/jquery.countdown.min.js"></script>
        <script async src="<?php echo e(url('/')); ?>/public/js/velocity.js"></script>

        <script src="<?php echo e(url('/')); ?>/public/js/script.js"></script>
        <link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css' />
        <link rel="stylesheet" href="<?php echo e(url('/')); ?>/public/css/font-awesome.min.css">
        <link href='<?php echo e(url("/")); ?>/public/css/bootstrap.min.css' rel='stylesheet' type='text/css' />
        <link href='<?php echo e(url("/")); ?>/public/css/style.css' rel='stylesheet' type='text/css' />
        

<script type='text/javascript'>
(function() {
var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
log.src = 'public/js/238538.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
})();
</script>

<!-- Google Tag Manager -->

<script>
(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MS77Z9');
</script>
<!-- End Google Tag Manager -->

<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106135697-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-106135697-1');
</script>



    </head>
    <body class="hideresponsive menu-repsonsive">
        <svg xmlns="http://www.w3.org/2000/svg" class="hidden"> 
    <symbol id="icon-account"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 612 792" enable-background="new 0 0 612 792" xml:space="preserve"><ellipse fill="#fff" cx="306" cy="280.2" rx="110.2" ry="112.3"></ellipse><path fill="#fff" d="M306,44.1c-152.1,0-275.4,123.3-275.4,275.4S153.9,594.9,306,594.9s275.4-123.3,275.4-275.4  S458.1,44.1,306,44.1z M475,487.8c-36.1-35-98.3-58.2-169-58.2s-133,23.1-169,58.2c-43-43.2-69.7-102.7-69.7-168.3  C67.3,187.9,174.4,80.8,306,80.8s238.7,107.1,238.7,238.7C544.7,385.1,518,444.6,475,487.8z"></path></svg></symbol>
    <symbol id="icon-cart-header"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 446.853 446.853"><g><path d="M444.274,93.36c-2.558-3.666-6.674-5.932-11.145-6.123L155.942,75.289c-7.953-0.348-14.599,5.792-14.939,13.708   c-0.338,7.913,5.792,14.599,13.707,14.939l258.421,11.14L362.32,273.61H136.205L95.354,51.179   c-0.898-4.875-4.245-8.942-8.861-10.753L19.586,14.141c-7.374-2.887-15.695,0.735-18.591,8.1c-2.891,7.369,0.73,15.695,8.1,18.591   l59.491,23.371l41.572,226.335c1.253,6.804,7.183,11.746,14.104,11.746h6.896l-15.747,43.74c-1.318,3.664-0.775,7.733,1.468,10.916   c2.24,3.184,5.883,5.078,9.772,5.078h11.045c-6.844,7.617-11.045,17.646-11.045,28.675c0,23.718,19.299,43.012,43.012,43.012   s43.012-19.294,43.012-43.012c0-11.028-4.201-21.058-11.044-28.675h93.777c-6.847,7.617-11.047,17.646-11.047,28.675   c0,23.718,19.294,43.012,43.012,43.012c23.719,0,43.012-19.294,43.012-43.012c0-11.028-4.2-21.058-11.042-28.675h13.432   c6.6,0,11.948-5.349,11.948-11.947c0-6.6-5.349-11.948-11.948-11.948H143.651l12.902-35.843h216.221   c6.235,0,11.752-4.028,13.651-9.96l59.739-186.387C447.536,101.679,446.832,97.028,444.274,93.36z M169.664,409.814   c-10.543,0-19.117-8.573-19.117-19.116s8.574-19.117,19.117-19.117s19.116,8.574,19.116,19.117S180.207,409.814,169.664,409.814z    M327.373,409.814c-10.543,0-19.116-8.573-19.116-19.116s8.573-19.117,19.116-19.117s19.116,8.574,19.116,19.117   S337.916,409.814,327.373,409.814z"/></g></svg></symbol>
    <symbol id="icon-search-filter"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 483.083 483.083"><g><path d="M332.74,315.35c30.883-33.433,50.15-78.2,50.15-127.5C382.89,84.433,298.74,0,195.04,0S7.19,84.433,7.19,187.85    S91.34,375.7,195.04,375.7c42.217,0,81.033-13.883,112.483-37.4l139.683,139.683c3.4,3.4,7.65,5.1,11.9,5.1s8.783-1.7,11.9-5.1    c6.517-6.517,6.517-17.283,0-24.083L332.74,315.35z M41.19,187.85C41.19,103.133,110.04,34,195.04,34    c84.717,0,153.85,68.85,153.85,153.85S280.04,341.7,195.04,341.7S41.19,272.567,41.19,187.85z"/></g></svg></symbol>
</svg>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <button type="button" class="navbar-toggle collapsed" id="trigger-mobile">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div id="box-wrapper">
            <header class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="flexbox-grid-default col-xs-12">
                <div class="flexbox-content flexbox-align-self-center">
                    <div class="support-phone-header">
                        <a href="tel:0938559501">
                            <svg class="svg-next-icon svg-next-icon-size-30" style="fill:#58b3f0">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-phone-header"></use>
                            </svg>
                            <span class="ml5">0968221530</span>
                        </a>
                    </div>
                </div>
                <div class="flexbox-content flexbox-align-self-center">
                    <div class="logo">
                        
                        <h1 class="hidden">MyShoes</h1>
                        
                        
                        <a href="<?php echo e(route('home')); ?>" title="MyShoes">
                            <img src="<?php echo e(url('/')); ?>/public/images/logo2.png" alt="" />
                        </a>
                        
                    </div>
                </div>
                <div class="flexbox-content flexbox-align-self-center">
                    <ul class="icon-control-header text-right">
                        <li class="search-header">
                            <div class="dropdown btn-group">
                                <a href="#" data-toggle="dropdown">
                                    <svg class="svg-next-icon svg-next-icon-size-24">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search-filter"></use>
                                    </svg>
                                </a>
                                <div class="dropdown-menu">
                                    <form action="<?php echo e(route('home.search')); ?>">
                                        <input type="text" class="form-control" name="key" placeholder="Tìm kiếm..." />
                                    </form>
                                </div>
                            </div>
                        </li>
                     
                        <li id="cart-target" class="cart">
                            <a href="<?php echo e(route('home.huong-cart')); ?>" class="cart " title="Giỏ hàng">
                                <svg class="svg-next-icon svg-next-icon-size-24">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cart-header"></use>
                                </svg>                  
                                <span id="cart-count"><?php echo e($totalitem); ?></span>
                            </a>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <?php if($auth->check()): ?>
                                    <a href="<?php echo e(route('home.huong-logout')); ?>" class="btn btn-sm btn-danger">Thoát</a>
                                    <?php else: ?>
                                        <b><a href="<?php echo e(route('home.huong-addkh')); ?>" class="btn btn-sm btn-danger">Đăng ký</a></b>
                                        <b><a href="<?php echo e(route('home.huong-adduser')); ?>" class="btn btn-sm btn-danger">Đăng nhập</a></b>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="navbar-main navbar navbar-default cl-pri">
    <!-- MENU MAIN -->
    <div class="container nav-wrapper">
        <div class="row">
            <div class="navbar-header">             
                <div class="flexbox-grid-default hidden-lg hidden-md hidden-sm">
                    <div class="flexbox-content text-center box-logo-mobile">
                        <div class="logo-mobile">
                            
                            <a href="//rossy-store.bizwebvietnam.net" title="Rossy Store">
                                <img src="<?php echo e(url('/')); ?>/public/images/logo.png" alt="#" />
                            </a>
                            
                        </div>
                    </div>
                    <div class="flexbox-auto">
                        <div class="mobile-menu-icon-wrapper">                  
                            <ul class="mobile-menu-icon clearfix">
                                <li class="search">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle icon-search" data-toggle="dropdown" aria-expanded="false">
                                            <svg class="svg-next-icon svg-next-icon-size-20">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search-filter"></use>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <div class="search-bar">
                                                <div class="">
                                                    <form class="col-md-12" action="/search">
                                                        <input type="text" name="q" placeholder="Tìm kiếm..." />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="cart-target" class="cart">
                                    <a href="<?php echo e(route('home.huong-cart')); ?>" class="cart " title="Giỏ hàng">
                                        <svg class="svg-next-icon svg-next-icon-size-20">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cart-header"></use>
                                        </svg>          
                                        <span id="cart-count"><?php echo e(route('home.huong-cart')); ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo e(route('home.huong-addkh')); ?>">
                                        <button type="submint">Đăng ký</button>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="nav navbar-nav clearfix flexbox-grid flexbox-justifyContent-center">
                            
                            
                            <li class=" current">
                                <a href="<?php echo e(route('home')); ?>" title="Trang chủ">Trang chủ</a>
                            </li>
                            
                            
                            
                            <li class="">
                                <a href="<?php echo e(route('home.gioithieu')); ?>" title="Giới thiệu">Giới thiệu</a>
                            </li>
                            
                              <?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            
                            <li class="">
                                <a href="<?php echo e(route('home.category',['id'=> $cat->id])); ?>" title="Giày nam"><?php echo e($cat->name); ?> <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu submenu-level1-children" role="menu">
                                    
                                    <?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="current">
                                        <a href="<?php echo e(route('home.category',['id'=> $c->id])); ?>" class="current" title="Lifestyle"><?php echo e($c->name); ?></a>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                            
                           
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            
                            
                            <li class="">
                                <a href="<?php echo e(route('home.tintuc')); ?>" title="Tin tức">Tin tức</a>
                            </li>
                            
                            
                            
                            <li class="">
                                <a href="<?php echo e(route('home.lienhe')); ?>" title="Liên hệ">Liên hệ</a>
                            </li>
                            
                            
                        </ul>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</nav>
<main>
    <div class="container">
        <form action="<?php echo e(route('home.search')); ?>" method="GET" class="form-inline" role="form">
        
            <div class="form-group">
                <select name="price" class="form-control" required>
                    <option value="">Chọn giá</option>
                    <option value="0-1000000">Từ 0 - 1000.000 đ</option>
                    <option value="1000000-2000000">Từ 1000.000 đ - 2000.000 đ</option>
                    <option value="2000000-3000000">Từ 2000.000 đ - 3000.000 đ</option>
                    <option value="3000000-30000000">Trên 3000.000 đ</option>
>
                </select>
            </div>
        
            
        
            <button type="submit" class="btn btn-primary">Lọc</button>
        </form>

    </div>
                <section class="slider-main mb15 container">
    <div id="slider-menu" class="slider-menu">
        <div class="owl-carousel">
            <div class="item active">
                <a href="/" title="Slide 1">
                    <picture>
                        <source media="(width: 100%)" srcset="<?php echo e(url('/')); ?>/public/images/slide_index_1.jpg">
                        <source media="(width: 100%) and (width: 100%)" srcset="<?php echo e(url('/')); ?>/public/images/slide_index_1.jpg">
                        <source media="(width: 100%)" srcset="public/images/slide_index_1.jpg">
                        <img src="<?php echo e(url('/')); ?>/public/images/slide_index_1.jpg" alt="Slide 1" width="1000px" />
                    </picture>
                </a>
            </div>
            <div class="item">
                <a href="/" title="Slide 2">
                    <picture>
                        <source media="(width: 100%)" srcset="<?php echo e(url('/')); ?>/public/images/slide_index_2.jpg">
                        <source media="(width: 100%) and (width:100%)" srcset="<?php echo e(url('/')); ?>/public/images/slide_index_2.jpg">
                        <source media="(width: 100%)" srcset="<?php echo e(url('/')); ?>/public/images/slide_index_2.jpg">
                        <img src="<?php echo e(url('/')); ?>/public/images/slide_index_2.jpg" alt="Slide 2" width="1000px" />
                    </picture>
                </a>
            </div>
        </div>
    </div>
</section>  

<div class="container mb30 hidden-xs">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12 pd-r-5 text-center pd5">
            <div class="box-content-service">
                <div class="icon-service">
                    <img src="<?php echo e(url('/')); ?>/public/images/icon-service-transport.png" alt="Giao hàng miễn phí" />
                </div>
                <div class="title-service">Giao hàng miễn phí</div>
                <div class="content-service">Tất cả sản phẩm đều được vận chuyển miễn phí</div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 pd-10 text-center pd5">
            <div class="box-content-service">
                <div class="icon-service">
                    <img src="<?php echo e(url('/')); ?>/public/images/icon-service-change.png" alt="Đổi trả hàng" />
                </div>
                <div class="title-service">Đổi trả hàng</div>
                <div class="content-service">Sản phẩm được phép đổi trả trong vòng 2 ngày</div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 pd-l-5 text-center pd5">
            <div class="box-content-service">
                <div class="icon-service">
                    <img src="<?php echo e(url('/')); ?>/public/images/icon-service-recieve-money.png" alt="Giao hàng nhận tiền" />
                </div>
                <div class="title-service">Giao hàng nhận tiền</div>
                <div class="content-service">Thanh toán đơn hàng bằng hình thức trực tiếp</div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 pd-l-5 text-center pd5">
            <div class="box-content-service">
                <div class="icon-service">
                    <img src="<?php echo e(url('/')); ?>/public/images/icon-service-phone.png" alt="Đặt hàng online" />
                </div>
                <div class="title-service">Đặt hàng online</div>
                <div class="content-service">096.822.1530</div>
            </div>
        </div>
    </div>
</div>
		<?php echo $__env->yieldContent('content'); ?>
		<!-- slide_8 -->
		<div id="blog-index" class="mb30">
    <div class="container group-index">
        <div class="row">
            <div class="col-xs-12">
                <div class="title-block">
                    <div class="wrap-content">
                        
                        <h2 class="title-group">Tin tức</h2>
                        
                        <div class="title-group-note">Tin tức được cập nhật thường xuyên</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 mb15">
                <div class="owl-carousel" id="blog-index">
                    <div class="item">
                        <div class="box-article-index">
                            <div class="article-image-index">
                                <a href="/quen-adidas-nmd-raw-pink-dat-do-di-doi-sneaker-mau-huong-nay-cung-yeu-khong-kem" title="Quên adidas NMD Raw Pink đắt đỏ đi, đôi sneaker màu hường này cũng yêu không kém mà giá rất phải chăng">
                                    <img src="<?php echo e(url('/')); ?>/public/images/2.jpg" alt="Quên adidas NMD Raw Pink đắt đỏ đi, đôi sneaker màu hường này cũng yêu không kém mà giá rất phải chăng" />
                                </a>
                            </div>
                            <div class="article-description-index">
                                <a href="/quen-adidas-nmd-raw-pink-dat-do-di-doi-sneaker-mau-huong-nay-cung-yeu-khong-kem" title="Quên adidas NMD Raw Pink đắt đỏ đi, đôi sneaker màu hường này cũng yêu không kém mà giá rất phải chăng"><h3 class="article-title">Quên adidas NMD Raw Pink đắt đỏ đi, đôi sneaker màu hường này cũng yêu không kém mà giá rất phải chăng</h3></a>
                                <div class="article-excerpt-index">
                                      Quên adidas NMD 'Raw Pink' đắt đỏ đi, đôi sneaker màu 'hường' này cũng yêu không kém mà giá rất phải chăng  
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="item">
                        <div class="box-article-index">
                            <div class="article-image-index">
                                <a href="/adidas-alphabounce-tro-nen-hao-nhoang-hon-bao-gio-het-voi-bo-suu-tap-phan-quang" title="Adidas AlphaBounce trở nên “hào nhoáng” hơn bao giờ hết với bộ sưu tập “ Phản quang”">
                                    <img src="<?php echo e(url('/')); ?>/public/images/adidas-alphabounce-reflective-pack-2.jpg" alt="Adidas AlphaBounce trở nên “hào nhoáng” hơn bao giờ hết với bộ sưu tập “ Phản quang”" />
                                </a>
                            </div>
                            <div class="article-description-index">
                                <a href="/adidas-alphabounce-tro-nen-hao-nhoang-hon-bao-gio-het-voi-bo-suu-tap-phan-quang" title="Adidas AlphaBounce trở nên “hào nhoáng” hơn bao giờ hết với bộ sưu tập “ Phản quang”"><h3 class="article-title">Adidas AlphaBounce trở nên “hào nhoáng” hơn bao giờ hết với bộ sưu tập “ Phản quang”</h3></a>
                                <div class="article-excerpt-index">
                                     Từ khi ra mắt với phần upper chỉ trên nền đen và trắng, adidas AlphaBounce giờ đây đã sẵn sàng kết hợp với những họa tiết sặc sỡ và chuyển đổi màu sắc XENO độc đáo. 
 AlphaBounce trở nên sáng sủa hơn bao giờ hết trong một gói...
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="item">
                        <div class="box-article-index">
                            <div class="article-image-index">
                                <a href="/adidas-climacool-02-17" title="Adidas ClimaCool 02/17">
                                    <img src="<?php echo e(url('/')); ?>/public/images/adidas-climacool-02-17-white-black-2.jpg" alt="Adidas ClimaCool 02/17" />
                                </a>
                            </div>
                            <div class="article-description-index">
                                <a href="/adidas-climacool-02-17" title="Adidas ClimaCool 02/17"><h3 class="article-title">Adidas ClimaCool 02/17</h3></a>
                                <div class="article-excerpt-index">
                                     Adidas đã mang lại hình ảnh ClimaCOOL cổ điển của họ từ đầu những năm 2000 vào mùa hè năm ngoái, nhưng không có nhiều ảnh hưởng như "ba sọc" mong đợi. Có lẽ đó là do sự nổi lên của NMD và Ultra Boost, nhưng bạn vẫn có...
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    <div class="item">
                        <div class="box-article-index">
                            <div class="article-image-index">
                                <a href="/nike-ra-mat-air-max-97-ultra-triple-black" title="Nike ra mắt Air Max 97 Ultra “Triple Black”">
                                    <img src="<?php echo e(url('/')); ?>/public/images/pebeast-com-2fimage-2f2017-2f08-2fnike-air-max-97-ultra-triple-black-1.jpg" alt="Nike ra mắt Air Max 97 Ultra “Triple Black”" />
                                </a>
                            </div>
                            <div class="article-description-index">
                                <a href="/nike-ra-mat-air-max-97-ultra-triple-black" title="Nike ra mắt Air Max 97 Ultra “Triple Black”"><h3 class="article-title">Nike ra mắt Air Max 97 Ultra “Triple Black”</h3></a>
                                <div class="article-excerpt-index">
                                     Tháng trước, Nike đã chia sẻ các phiên bản "hiện đại hóa" của Air Max 97 Ultra được người hâm mộ yêu thích, dự kiến ​​sẽ phát hành vào mùa thu. Có vẻ như Nike đã phải ra mắt sớm hơn, với "Triple Black" trong số những phiên bản...
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <div class="col-xs-12">
                <div class="text-center">
                    <a href="/tin-tuc" class="btn btn-view-more">Xem thêm</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearbox"></div>


<div class="container mb15 group-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="title-block">
                <div class="wrap-content">
                    
                    <h2 class="title-group">Thương hiệu</h2>
                    
                    <div class="title-group-note">Thương hiệu nổi bật của chúng tôi</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="brand-carousel">
            <div class="owl-carousel">
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_1.png" alt="logo 1" />
                    </a>
                </div>
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_2.png" alt="logo 2" />
                    </a>
                </div>
                
                
                
                
                
                
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_3.png" alt="logo 3" />
                    </a>
                </div>
                
                
                
                
                
                
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_4.png" alt="logo 4" />
                    </a>
                </div>
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_5.png" alt="logo 5" />
                    </a>
                </div>
                
                
                
                
                
                
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_6.png" alt="logo 6" />
                    </a>
                </div>
                
                
                
                
                
                
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_7.png" alt="logo 7" />
                    </a>
                </div>
                
                
                
                
                
                
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_8.png" alt="logo 8" />
                    </a>
                </div>
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_9.png" alt="logo 9" />
                    </a>
                </div>
                
                
                
                
                
                
                
                <div class="item">
                    <a href="/">
                        <img src="<?php echo e(url('/')); ?>/public/images/partner_10.png" alt="logo 10" />
                    </a>
                </div>
                
                
            </div>
        </div>
    </div>
</div>

            </main>
            <footer>
    <div class="footer-center">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pd5">
                    <div class="box-footer-colum">
                        <h4 class="">Liên hệ</h4>
                        <ul class="footer-list-menu">
                            
                            <li class="footer-address">Khu đô thị Pháp Vân-Tứ Hiệp-Thanh Trì-Hà Nội</li>
                            
                            
                            <li class="fooer-phone">
                                <label class="mr5">Điện thoại: </label><span><a href="tel:0968221530">0968221530</a></span>
                            </li>
                            
                            
                            <li class="footer-email">
                                <label class="mr5">Email: </label><span><a href="lilanhuong01@gmail.com">lilanhuong01@gmail.com</a></span>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pd5">
                    <div class="box-footer-colum">
                        <h4 class="">Chính sách hỗ trợ</h4>
                        <ul class="footer-list-menu">
                            
                            
                            <li>
                                <a href="/huong-dan-thanh-toan" title="Hướng dẫn thanh toán">Hướng dẫn thanh toán</a>
                            </li>
                            
                            <li>
                                <a href="/giao-hang-tan-nha" title="Giao hàng tận nhà">Giao hàng tận nhà</a>
                            </li>
                            
                            <li>
                                <a href="/chinh-sach-doi-tra" title="Chính sách đổi trả">Chính sách đổi trả</a>
                            </li>
                            
                            <li>
                                <a href="/chinh-sach-bao-mat" title="Chính sách bảo mật">Chính sách bảo mật</a>
                            </li>
                                
                            
                        </ul>
                    </div>
                </div>
                
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pd5">
                    <div class="box-footer-colum">
                        <h4 class="">Liên kết</h4>
                        <div class="social">
                            <ul class="footer-list-menu">
                                <li>Hãy kết nối với chúng tôi.</li>
                            </ul>
                            <ul class="list-unstyled clearfix">
                                
                                <li class="facebook">
                                    <a target="_blank"  href="/"  class="fa fa-facebook"></a>
                                </li>
                                
                                
                                <li class="twitter">
                                    <a class="fa fa-twitter" target="_blank" href="/" ></a>
                                </li>
                                
                                
                                <li class="google-plus">
                                    <a class="fa fa-google-plus" target="_blank" href="/"></a>
                                </li>
                                
                                
                                <li class="rss">
                                    <a class="fa fa-instagram" target="_blank" href="/"></a>
                                </li>
                                
                                
                                <li class="youtube">
                                    <a class="fa fa-youtube" target="_blank" href="/"></a>
                                </li>
                                
                            </ul>
                            
                            <div class="dkbocongthuong">
                                <img src="<?php echo e(url('/')); ?>/public/images/dkbocongthuong.png" alt="Bộ công thương" />
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pd5">
                    <div class="box-footer-colum">
                        <h4 class="">Fanpage</h4>
                        <div class="fb-page" 
                          data-href="https://www.facebook.com/LeoShop-1535781579766804/"
                          data-width="380" 
                          data-hide-cover="false"
                          data-show-facepile="true"></div>


                </div>
                
            </div>
        </div>
    </div>
    
    <div class="footer-bottom">
        <div class="pd5 text-center">
            <div class="footer-copyright">
                
                <div class="inline-block">&copy; Bản quyền thuộc về <a href="https://www.facebook.com/lanhuong.ly.5" target="_blank">Lan Hương</a>. </div>
                <div class="inline-block">Cung cấp bởi <a href="https://www.facebook.com/LeoShop-1535781579766804/" target="_blank">Lan Hương</a>.</div>                
                
            </div>
        </div>
    </div>
    
</footer>
            <div class="hotline-mobile hidden-lg">
                <a href="tel:">
                    <svg class="svg-next-icon svg-next-icon-size-40" style="fill:#58b3f0">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-phone-header"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div id="menu-mobile">
    <div class="clearfix">
        <div class="account_mobile" style="">
            <div class="text-center">
                <svg class="svg-next-icon svg-next-icon-size-50" style="fill:#fff;">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-account"></use>
                </svg>
            </div>
            <ul class="account_text text-center">
                
                <li><a class="register_account" href="/account/register" title="Đăng ký">Đăng ký&nbsp&nbsp</a></li>
                <li>|</li>
                <li><a class="login_account" href="/account/login" title="Đăng nhập">&nbsp&nbspĐăng nhập</a></li>
                
            </ul>
        </div>
        <ul class="menu-mobile">
            
            
            <li class="current">
                <a href="/" title="Trang chủ">Trang chủ</a>
            </li>
            
            
            
            <li class="">
                <a href="/gioi-thieu" title="Giới thiệu">Giới thiệu</a>
            </li>
            
            
            
            <li class="">
                <a href="javascript:void(0);" title="Giày nam">Giày nam <i class="fa fa-angle-right"></i></a>
                <ul class="dropdown-menu submenu-level1-children" role="menu">
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Lifestyle">Lifestyle</a>
                    </li>
                    
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Running">Running</a>
                    </li>
                    
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Basketball">Basketball</a>
                    </li>
                    
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Football">Football</a>
                    </li>
                    
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Gym & Training">Gym & Training</a>
                    </li>
                    
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Skateboarding">Skateboarding</a>
                    </li>
                    
                    
                    
                    <li class="current">
                        <a href="/" class="current" title="Tennis">Tennis</a>
                    </li>
                    
                    
                </ul>
            </li>
            
            
            
            <li class="">
                <a href="/giay-nu" title="Giày nữ">Giày nữ</a>
            </li>
            
            
            
            <li class="">
                <a href="/tin-tuc" title="Tin tức">Tin tức</a>
            </li>
            
            
            
            <li class="">
                <a href="/lien-he" title="Liên hệ">Liên hệ</a>
            </li>
            
            
        </ul>
    </div>  
</div>
        
        <div class="wrapper-quickview row">
    <div class="">
        <div class="quickview-image">
            <img src='<?php echo e(url("/")); ?>/public/images/image-empty.jpg' alt="Xem nhanh">
        </div>
        <div id="quickview-sliderproduct">
            <div class="quickview-slider owl-carousel"></div>           
        </div>
    </div>
    <div class="">
        <form id="form-quickview" method="post" action="/cart/add">
            <div class="quickview-information">
                <div class="quickview-close">
                    <a href="javascript:void(0);"><i class="fa fa-times"></i></a>
                </div>
                <a href="#" class="quickview-title" title=""><h3></h3></a>
                <div class="clearfix">
                    <span class="pull-left product-sku"></span>
                </div>
                <div class="quickview-price">
                    <span></span><del></del>
                </div>

                <div class="quickview-variants variant-style clearfix">
                    
                    <div class="selector-wrapper opt1-quickview flexbox-grid-default flexbox-alignItems-center">
                        <label></label>
                        <ul class="data-opt1-quickview clearfix style-variant-template">

                        </ul>
                    </div>
                    <div class="selector-wrapper opt2-quickview flexbox-grid-default flexbox-alignItems-center">
                        <label></label>
                        <ul class="data-opt2-quickview clearfix style-variant-template">

                        </ul>
                    </div>
                    <div class="selector-wrapper opt3-quickview flexbox-grid-default flexbox-alignItems-center">
                        <label></label>
                        <ul class="data-opt3-quickview clearfix style-variant-template">

                        </ul>
                    </div>
                    <style>
                        .selector-wrapper:not(.opt1):not(.opt1-quickview):not(.opt2):not(.opt2-quickview):not(.opt3):not(.opt3-quickview) {
                            display: none;
                        }
                    </style>
                    
                    <input type="hidden" name="variantId" value="">
                    <div class="selector-wrapper price-contact">
                        <label>Số lượng</label>
                        <div class="input-group mb15">
                            <input type="button" value="-" onclick="Nobita.minusQuantity()" class="qty-btn">
                            <input type="text" id="quantity" name="quantity" value="1" min="1" class="quantity-selector">
                            <input type="button" value="+" onclick="Nobita.plusQuantity()" class="qty-btn">
                        </div>
                    </div>
                    <select name="id" class="" id="quickview-select"></select>
                    <div class="clearfix price-contact">
                        <button class="btn-style-add addnow-quickview">
                            <span class="icon_cart_btn"></span>
                            <span>Thêm vào giỏ</span>
                        </button>
                    </div>
                </div>
                <div class="quickview-description"></div>
            </div>
        </form>
    </div>
</div>


        
        <div class="back-to-top">
            <a href="javascript:void(0);">
                <svg class="svg-next-icon svg-next-icon-size-30" style="fill:#58b3f0">
                    <use xlink:href="#icon-scrollUp-bottom"></use>
                </svg>
            </a>
        </div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=483651425338440';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
                
    </body>
</html>