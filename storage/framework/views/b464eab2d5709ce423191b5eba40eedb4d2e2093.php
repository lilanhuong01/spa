<?php $__env->startSection('content'); ?>
    <div class="col-md-5">
    <?php if($auth->check()): ?>
        <?php if(count($carts)): ?>
            <form action="" method="POST" role="form">
            <legend style="text-align: center;">Đặt Hàng</legend>
            <div class="form-group">
                <label for="">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Nhập Name" value="<?php echo e($auth->user()->name); ?>">
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Nhập Email" value="<?php echo e($auth->user()->email); ?>">
            </div>
            <div class="form-group">
                <label for="">Phone</label>
                <input type="text" class="form-control" name="phone" placeholder="Nhập Số Điện Thoại" value="<?php echo e($auth->user()->phone); ?>">
            </div>
            <div class="form-group">
                <label for="">Address</label>
                <input type="text" class="form-control" name="address" placeholder="Nhập Địa Chỉ" value="<?php echo e($auth->user()->address); ?>">
            </div>
            <div class="form-group">
                <label for="">Phương thưc thanh toán</label>
                <select name="pay" id="inputPay" class="form-control" required>
                    <option value="">Chọn phương thưc thanh toán</option>
                    <option value="Tại cửa hàng">Tại cửa hàng</option>
                    <option value="Qua tài khoản ngân hàng">Qua tài khoản ngân hàng</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Phương thức vận chuyển</label>
                <select name="ship" id="inputPay" class="form-control" required>
                    <option value="">Chọn phương thưc vận chuyển</option>
                    <option value="Tại cửa hàng">Tại cửa hàng</option>
                    <option value="Chuyển qua bưu điện">Chuyển qua bưu điện</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Ghi chú thêm</label>
                <textarea name="note" class="form-control" rows="3"></textarea>
            </div>
            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
            <button type="submit" class="btn btn-primary">Đặt Hàng</button>
            <button type="submit" class="btn btn-primary"><a href="<?php echo e(route('home.huong-hoadon')); ?>" title="">Hóa Đơn</a> </button>
        </form>
        <?php else: ?>
            <div class="jumbotron">
                <div class="container">
                    <h2>Giỏ hàng bị rỗng</h2>
                    <p>Vui lòng quay lại mua hàng ...</p>
                    <p>
                        <a class="btn btn-primary btn-lg" href="<?php echo e(route('home')); ?>">Mua hàng</a>
                    </p>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <form action="<?php echo e(route('home.huong-adduser')); ?>" method="POST" role="form" style="width: 300px;margin: 20px auto; ">
     <legend>Đăng Nhập</legend>
   <?php if(Session::has('info')): ?>
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <?php echo e(Session::get('info')); ?>

      </div>
    <?php endif; ?>
     <div class="form-group">
       <label for="">Tài Khoản</label>
       <input type="email" class="form-control" name="email" placeholder="Nhập Email">
       <?php if($errors->has('email')): ?>
        <div class="help-block">
          <b style="color: red">Thông Tin Tài Khoản Sai</b>
          
        </div>
       <?php endif; ?>
     </div>
     <div class="form-group">
       <label for="">Mật Khẩu</label>
       <input type="password" class="form-control" name="password" placeholder="Nhập Mật Khẩu">
       <?php if($errors->has('password')): ?>
       <div class="help-block">
         <b style="color: red">Thông Tin Tài Khoản Sai</b>
       </div>
       <?php endif; ?>
     </div>
     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
     <button type="submit" class="btn btn-primary">Submit</button>
   </form>
    <?php endif; ?>
    </div>
    <div class="col-md-7">
        <table class="table table-hover" >
        <thead>
            <tr>
                <th width="10">STT</th>
                <th width="10">Ảnh</th>
                <th width="10">Tên Sản Phẩm</th>
                <th width="10">Giá</th>
                <th width="10">Số Lượng</th>
            </tr>
        </thead>
        <tbody>
            <?php ($n = 1); ?>
            <?php if(count($carts)): ?> <?php $__currentLoopData = $carts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($n); ?></td>
                <td  width="10">
                    <img src="<?php echo e(url('uploads')); ?>/<?php echo e($cart['image']); ?> "  width="70" >
                </td>
                <td><?php echo e($cart['name']); ?></td>
                <td><?php echo e(number_format($cart['price'],0,'',',')); ?> đ</td>
                <td>
                    <?php echo e($cart['amount']); ?>

                </td>
            
            </tr>
            <?php ($n ++); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
        </tbody>
        <div>
            <tr>
                <th colspan="3">Tổng số</th>
                <th><?php echo e(number_format($total,0,'',',')); ?> đ</th>
                <th colspan="2"><?php echo e($totalitem); ?> SL   </th>
            </tr>
        </div>
    </table>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>