<?php $__env->startSection('content'); ?>

		<div id="slide_3">
			<section class="box-product">
				<div class="container">
					<div class="title-box-product">
						<h2>NHỮNG SẢN PHẨM</h2>
						<h3>CÓ THỂ LÀM ĐƯỢC SAU KHÓA HỌC</h3>
					</div>
					<div class="slide-box-product">
						<div class="owl-carousel owl-theme owl-product-home">
							<div class="item">
								<div class="item-product">
									<div class="item-img">
										<a href="#" title="">
											<img class="img-responsive" src="images/img-product.jpg" alt="">
										</a>
									</div>
									<div class="item-description">

									</div>
									<h3 class="item-des-title"><a href="#">WEB THƯƠNG MẠI ĐIỆN TỬ</a></h3>
								</div>
							</div>
							<div class="item">
								<div class="item-product">
									<div class="item-img">
										<a href="#" title="">
											<img class="img-responsive" src="images/img-product.jpg" alt="">
										</a>
									</div>
									<div class="item-description">

									</div>
									<h3 class="item-des-title"><a href="#">WEB THƯƠNG MẠI ĐIỆN TỬ</a></h3>
								</div>
							</div>
							<div class="item">
								<div class="item-product">
									<div class="item-img">
										<a href="#" title="">
											<img class="img-responsive" src="images/img-product.jpg" alt="">
										</a>
									</div>
									<div class="item-description">

									</div>
									<h3 class="item-des-title"><a href="#">WEB THƯƠNG MẠI ĐIỆN TỬ</a></h3>
								</div>
							</div>
							<div class="item">
								<div class="item-product">
									<div class="item-img">
										<a href="#" title="">
											<img class="img-responsive" src="images/img-product.jpg" alt="">
										</a>
									</div>
									<div class="item-description">

									</div>
									<h3 class="item-des-title"><a href="#">WEB THƯƠNG MẠI ĐIỆN TỬ</a></h3>
								</div>
							</div>
							<div class="item">
								<div class="item-product">
									<div class="item-img">
										<a href="#" title="">
											<img class="img-responsive" src="images/img-product.jpg" alt="">
										</a>
									</div>
									<div class="item-description">

									</div>
									<h3 class="item-des-title"><a href="#">WEB THƯƠNG MẠI ĐIỆN TỬ</a></h3>
								</div>
							</div>
						</div>
					</div><!-- .slide-box-product -->
				</div>
			</section><!-- .box-product -->
		</div>
		<!-- slide_3 -->
		<div id="slide_4">
			<section class="content-program">
				<div class="container">
					<div class="title-content-program">
						<h2>NỘI DUNG CHƯƠNG TRÌNH</h2>
					</div>
					<div class="tabs-content-program clearfix">
						<ul class="nav nav-pills nav-stacked col-md-2">
							<li class="active"><a href="#gd_1" data-toggle="pill">GIAI ĐOẠN 1</a></li>
							<li><a href="#gd_2" data-toggle="pill">GIAI ĐOẠN 2</a></li>
							<li><a href="#gd_3" data-toggle="pill">GIAI ĐOẠN 3</a></li>
							<li><a href="#gd_4" data-toggle="pill">GIAI ĐOẠN 4</a></li>
							<li><a href="#gd_5" data-toggle="pill">GIAI ĐOẠN 5</a></li>
						</ul>
						<div class="tab-content col-md-10">
							<div class="tab-pane active" id="gd_1">
								<h4>Pane A</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
							</div>
							<div class="tab-pane" id="gd_2">
								<h4>Pane B</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
							</div>
							<div class="tab-pane" id="gd_3">
								<h4>Pane C</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
							</div>
							<div class="tab-pane" id="gd_4">
								<h4>Pane D</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
							</div>
							<div class="tab-pane" id="gd_5">
								<h4>Pane D</h4>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
							</div>
						</div><!-- tab content -->
					</div><!-- .tabs-content-program -->
				</div>
			</section><!-- .content-program -->
		</div>
		<!-- slide_4 -->
		<div id="slide_5">
			<section class="box-select">
				<div class="bg-box-select">
					<div class="container">
						<div class="title-box-select">
							<h3>TẠI SAO NÊN CHỌN</h3>
							<h2>BACHKHOA-APTECH</h2>
						</div>
						<div class="row">
							<div class="col-sm-3 item-select">
								<div class="item-select-icon">
									<img class="img-responsive" src="images/box-icon.png" alt="">
								</div>
								<div class="item-select-title">
									<h3>Công nghệ đào tạo hàng đầu</h3>
								</div>
								<div class="item-select-content">
									<p>Phương pháp giảng dạy Blended Learning & AMEDA chú trọng kỹ năng thực hành</p>
								</div>
								<div class="item-select-bottom">
									<p class="item-bot-number">30.000</p>
									<p class="item-bot-text">Học viên đã tốt nghiệp</p>
								</div>
							</div>
							<div class="col-sm-3 item-select">
								<div class="item-select-icon">
									<img class="img-responsive" src="images/bag-icon.png" alt="">
								</div>
								<div class="item-select-title">
									<h3>Thực hành trên nền tảng DN</h3>
								</div>
								<div class="item-select-content">
									<p>Thực hành tại Công ty phần mềm Bachkhoa-Software</p>
								</div>
								<div class="item-select-bottom">
									<p class="item-bot-number">60</p>
									<p class="item-bot-text">Đối tác doanh nghiệp</p>
								</div>
							</div>

							<div class="col-sm-3 item-select">
								<div class="item-select-icon">
									<img class="img-responsive" src="images/shield-icon.png" alt="">
								</div>
								<div class="item-select-title">
									<h3>Bảo hành học tập chọn đời</h3>
								</div>
								<div class="item-select-content">
									<p>Duy trì mãi mãi lớp học công nghệ cho các học viên đã tốt nghiệp</p>
								</div>
								<div class="item-select-bottom">
									<p class="item-bot-number">15</p>
									<p class="item-bot-text">Năm kinh nghiệm đào tạo</p>
								</div>
							</div>

							<div class="col-sm-3 item-select">
								<div class="item-select-icon">
									<img class="img-responsive" src="images/user-icon.png" alt="">
								</div>
								<div class="item-select-title">
									<h3>Cam kết hỗ trợ việc làm</h3>
								</div>
								<div class="item-select-content">
									<p>Giới thiệu việc làm cho các học viên sau khi tốt nghiệp khóa học</p>
								</div>
								<div class="item-select-bottom">
									<p class="item-bot-number">28.000</p>
									<p class="item-bot-text">Học viên đã có việc làm</p>
								</div>
							</div>
						</div><!-- .row -->
					</div>
				</div>
			</section><!-- .box-select -->
		</div>
		<!-- slide_5 -->
		<div id="slide_6">
			<section class="model-bkap">
				<div class="container">
					<div class="row">
						<div class="col-sm-7 model-bkap-accordion">
							<div class="title-model-bkap">
								<h3>MÔ HÌNH HỌC TẬP TẠI</h3>
								<h2>BACHKHOA-APTECH</h2>
							</div>
							<div class="accordion-model-bkap">
								<div class="panel-group" id="accordion">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#step1">
													<span class="panel-title-icon"><img src="images/search-for-staff-1-.png" alt=""></span>
													Bước 1: Nhập học
												</a>
											</h4>
										</div>
										<div id="step1" class="panel-collapse collapse in">
											<div class="panel-body">
												- Học viên được hướng dẫn chi tiết mô hình "LÀM TRƯỚC - HỌC SAU". <br>
												- Học viên điền đầy đủ thông tin vào form đăng ký và được ký cam kết chất lượng đào tạo. <br>
											- Học viên đăng ký lịch làm việc tại Doanh nghiệp</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#step2" class="collapsed">
													<span class="panel-title-icon"><img src="images/printing-calculator.png" alt=""></span>
												Bước 2: Làm viêc tại doanh nghiệp</a>
											</h4>
										</div>
										<div id="step2" class="panel-collapse collapse">
											<div class="panel-body">
												- Học viên nghiên cứu và làm việc tại Doanh nghiệp trước khi học ít nhất 01 tuần. <br>
												- Học viên được hướng dẫn những yêu cầu tuyển dụng từ phía Doanh nghiệp.<br>
												- Học viên được người Quản lý dự án của Doanh nghiệp trực tiếp hướng dẫn kiến thức và kỹ năng thực tiễn.<br> 
												- Học viên làm báo cáo tổng hợp gửi phòng CTSV Bachkhoa-Aptech.
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#step3" class="collapsed">
													<span class="panel-title-icon"><img src="images/coding.png" alt=""></span>
												Bước 3: Học tập tại BKAP</a>
											</h4>
										</div>
										<div id="step3" class="panel-collapse collapse">
											<div class="panel-body">
												- Chương trình đào tạo hoàn toàn dựa trên yêu cầu tuyển dụng Doanh nghiệp và cập nhật xu thế công nghệ Thế giới. <br>
												- Học viên được giảng viên hướng dẫn phương pháp học tập hiệu quả dựa trên khả năng của từng học viên.
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#step4" class="collapsed">
													<span class="panel-title-icon"><img src="images/businessman-working-online-with-a-laptop.png" alt=""></span>
												Bước 4: Thực tập tại doanh nghiệp</a>
											</h4>
										</div>
										<div id="step4" class="panel-collapse collapse">
											<div class="panel-body">
												- Học viên được trải nghiệm môi trường thực tiễn tại Doanh nghiệp. <br>
												- Học viên có thể có lương trong quá trình thực tập tại Doanh nghiệp.<br>
												- Học viên nhận chứng nhận thực tập Doanh Nghiệp.
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#step5" class="collapsed">
													<span class="panel-title-icon"><img src="images/graduate-student-avatar.png" alt=""></span>
												Bước 5: Tốt nghiệp</a>
											</h4>
										</div>
										<div id="step5" class="panel-collapse collapse">
											<div class="panel-body">
												- Học viên bảo vệ đồ án thực tiễn cuối khóa. <br>
												- Học viên được giới thiệu việc làm tới Doanh nghiệp phù hợp. <br>
												- Học viên được trao chứng nhận, bằng cấp
											</div>
										</div>
									</div>
								</div>
							</div><!-- .accordion-model-bkap -->
						</div><!-- .model-bkap-accordion -->
						<div class="col-sm-5 model-bkap-slider">
							<div class="slider model-bkap-slick">
								<div class="item">
									<img class="img-responsive" src="images/slider-img-1.jpg" alt="">
								</div>
								<div class="item">
									<img class="img-responsive" src="images/slider-img-1.jpg" alt="">
								</div>
								<div class="item">
									<img class="img-responsive" src="images/slider-img-1.jpg" alt="">
								</div>
								<div class="item">
									<img class="img-responsive" src="images/slider-img-1.jpg" alt="">
								</div>
								<div class="item">
									<img class="img-responsive" src="images/slider-img-1.jpg" alt="">
								</div>
								<div class="item">
									<img class="img-responsive" src="images/slider-img-1.jpg" alt="">
								</div>
							</div>
						</div><!-- .model-bkap-slider -->
					</div><!-- .row -->
					<hr>
				</div>
			</section><!-- .model-bkap -->
		</div>
		<!-- slide_6 -->
		<div id="slide_7">
			<section class="box-feed-back">
				<div class="bg-box-feed-back">
					<div class="container">
						<div class="title-box-feed-back">
							<h2>CẢM NHẬN HỌC VIÊN</h2>
						</div>
						<div class="content-box-feed-back">
							<div class="slide-feed-back">
								<div class="owl-carousel owl-theme owl-feed-back">
									<div class="item">
										<div class="item-feed-back">
											<div class="item-fb-img text-center">
												<a href="#" title="">
													<img class="img-responsive" src="images/avatar.png" alt="">
												</a>
												<p>Bella</p>
											</div>
											<div class="item-fb-description">
												<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley </p>
											</div>
										</div><!-- .item-feed-back -->
									</div><!-- .item -->
									<div class="item">
										<div class="item-feed-back">
											<div class="item-fb-img text-center">
												<a href="#" title="">
													<img class="img-responsive" src="images/avatar.png" alt="">
												</a>
												<p>Bella</p>
											</div>
											<div class="item-fb-description">
												<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley </p>
											</div>
										</div><!-- .item-feed-back -->
									</div><!-- .item -->
									<div class="item">
										<div class="item-feed-back">
											<div class="item-fb-img text-center">
												<a href="#" title="">
													<img class="img-responsive" src="images/avatar.png" alt="">
												</a>
												<p>Bella</p>
											</div>
											<div class="item-fb-description">
												<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley </p>
											</div>
										</div><!-- .item-feed-back -->
									</div><!-- .item -->
									<div class="item">
										<div class="item-feed-back">
											<div class="item-fb-img text-center">
												<a href="#" title="">
													<img class="img-responsive" src="images/avatar.png" alt="">
												</a>
												<p>Bella</p>
											</div>
											<div class="item-fb-description">
												<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley </p>
											</div>
										</div><!-- .item-feed-back -->
									</div><!-- .item -->
									<div class="item">
										<div class="item-feed-back">
											<div class="item-fb-img text-center">
												<a href="#" title="">
													<img class="img-responsive" src="images/avatar.png" alt="">
												</a>
												<p>Bella</p>
											</div>
											<div class="item-fb-description">
												<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley </p>
											</div>
										</div><!-- .item-feed-back -->
									</div><!-- .item -->
								</div>
							</div>
						</div><!-- .content-box-feed-back -->
					</div><!-- .container -->
				</div><!-- .bg-box-feed-back -->
			</section><!-- .box-fedd-back -->
		</div>
		<!-- slide_7 -->
		<div id="slide_8">
			<section class="box-register">
				<div class="bg-box-register">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 box-register-info">
								<div class="title-box-register">
									<h2>ĐĂNG KÝ THÔNG TIN</h2>
								</div>
								<div class="box-register-description">
									<p>Để nhận thông tin tư vấn về chương trình đào tạo, tài liệu học tập, thông tin tuyển dụng PHP. Vui lòng điền đầy đủ thông tin, chúng tôi sẽ liên hệ với bạn sớm nhất.</p>
								</div>
								<div class="title-box-register">
									<h2>LỊCH KHAI GIẢNG</h2>
								</div>
								<div class="box-register-description">
									<p>Chỉ còn 5 ngày</p>
								</div>
							</div>
							<div class="col-sm-5 box-register-form pull-right">
								<form>
									<div class="form-horizontal form-banner">
										<h2>ĐĂNG KÝ NGAY</h2>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Họ và tên">
											</div>
										</div>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Email">
											</div>
										</div>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Số điện thoại">
											</div>
										</div>
										<div class="form-group">
											<div>
												<input class="form-control" type="text" name="" placeholder="Địa chỉ">
											</div>
										</div>
										<div class="form-group">
											<div class="text-center">
												<button class="btn btn-warning" type="submit">GỬI</button>
											</div>
										</div>
									</div><!-- .form-horizontal -->
								</form>
							</div>
						</div><!-- .row -->
					</div>
				</div><!-- .bg-box-register -->
			</section><!-- .register -->
			<section class="comment-facebook">

			</section><!-- .comment-facebook -->
			<section class="box-partner">
				<div class="title-box-partner">
					<h2>ĐỐI TÁC TUYỂN DỤNG</h2>
				</div>
				<div class="container">
					<div class="slider-partner">
						<div class="owl-carousel owl-theme owl-slide-partner">
							<div class="item">
								<img class="img-responsive" src="images/logo-partner-viettel.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="images/logo-partner-nstech.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="images/logo-partner-azebiz.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="images/logo-partner-gameloft.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="images/logo-partner-tinhvan.png" alt="">
							</div>
							<div class="item">
								<img class="img-responsive" src="images/logo-partner-vatgia.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</section><!-- .box-partner -->
		</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>