
  <?php echo $__env->make('admin.layouts.backend-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('admin.layouts.backend-aside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="content-wrapper">
    <?php echo $__env->make('admin.layouts.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $__env->yieldContent('title'); ?>
        <small><?php echo $__env->yieldContent('sub-title'); ?></small>
      </h1>
      <ol class="breadcrumb">
       <?php echo $__env->yieldContent('breadcrumb'); ?>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="btn-too-bar pull-left">
                  <?php echo $__env->yieldContent('tool-bar'); ?>
              </div>
              <div class="box-tools pull-right">
                <form action="" method="GET" class="form-inline">
                <div class="input-group form-group">
                  <input type="text" name="search" class="form-control" placeholder="Từ khóa tìm kiếm..." value="<?php echo e(old('search')); ?>" />
                  <span class="input-group-btn">
                    <button type="submit"  id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                  </span>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body">
          
            <div class="panel panel-default">
            <div class="panel-body">
              <?php echo $__env->yieldContent('content'); ?>
            </div>
          </div>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <?php echo $__env->yieldContent('footer'); ?>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<?php echo $__env->make('admin.layouts.backend-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>