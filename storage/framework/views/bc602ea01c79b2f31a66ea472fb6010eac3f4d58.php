<?php $__env->startSection('content'); ?>
<div class="container">
<div class="col-md-5">
<form action="<?php echo e(route('home.loginkh')); ?>" method="post">
      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Tài khoản">

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <?php if($errors->has('username')): ?>
          <strong><font color='red'>Vui lòng nhập tài khoản</font></strong>
        <?php endif; ?>
      </div>

      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          <?php if($errors->has('password')): ?>
          <strong><font color='red'>Vui lòng nhập mật khẩu</font></strong>
        <?php endif; ?>
      </div>
      <div class="form-group has-feedback">
        <div class="col-xs-7">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> Ghi nhớ mật khẩu
            </label>
          </div>
        </div>
      <!-- /.col -->
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Đăng nhập</button>
        </div>
      <!-- /.col -->
      </div>
      
      
      
    </form>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.layoutkh', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>