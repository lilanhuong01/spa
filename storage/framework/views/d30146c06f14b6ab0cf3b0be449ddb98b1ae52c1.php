<?php $__env->startSection('giohang'); ?>
	<li class="checkout"><a href="<?php echo e(route('home.huong-cart')); ?>">Giỏ Hàng</a>
<?php $__env->stopSection(); ?>


<!-- <?php $__env->startSection('category'); ?>

 	  <div class="categories col-md-3">
    <h3>DANH MỤC SẢN PHẨM</h3>
    <ul>
    <?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><a href="<?php echo e(route('home.category',['id'=>$cat->id])); ?>"><?php echo e($cat->name); ?></a></li> 
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
  </div>
 <?php $__env->stopSection(); ?>
  <?php $__env->startSection('sanpham'); ?>

 <?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

 	

 		<li class=""><a href="<?php echo e(route('home.category',['id'=>$cat->id])); ?>"><?php echo e($cat->name); ?></a></li>
      
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     
 <?php $__env->stopSection(); ?>

 -->
<?php $__env->startSection('content'); ?>
<div class="container">
	<div class="col-md-3" style="border-right: 1px solid black">
		<h4 style="color: red">DANH MỤC SẢN PHẨM</h4>
		<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      		<li style="border-bottom: 1px dotted red"><a href="<?php echo e(route('home.category',['id'=>$cat->id])); ?>"><?php echo e($cat->name); ?></a></li> 
			<?php $__currentLoopData = $cat->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		        <li class="current" style="border-bottom: 1px dotted red; padding-top: 5px">
		            <a href="<?php echo e(route('home.category',['id'=> $c->id])); ?>" class="current" title="Lifestyle"><?php echo e($c->name); ?></a>
		        </li>
		    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


      	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
<div class="col-md-9">
<table class="table table-hover" >
	<thead>
		<tr>
			<th width="10">STT</th>
			<th width="10">Ảnh</th>
			<th width="10">Tên Sản Phẩm</th>
			<th width="10">Giá</th>
			<th width="10">Số Lượng</th>
			<th width="10">Size</th>
			<th width="10">Tác Vụ</th>
		</tr>
	</thead>
	<tbody>
		<?php ($n = 1); ?>
		<?php if(count($carts)): ?> <?php $__currentLoopData = $carts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td><?php echo e($n); ?></td>
			<td  width="10">
				<img src="<?php echo e(url('uploads')); ?>/<?php echo e($cart['image']); ?> "  width="70" >
			</td>
			<td><?php echo e($cart['name']); ?></td>
			<td><?php echo e(number_format($cart['price'],0,'',',')); ?><sup style="color: red">đ</sup></td>
			<td>
				<form action="<?php echo e(route('home.huong-updatecart',['id'=>$cart['id']])); ?>" method="POST" role="form">
						<input type="number" name="amount" value="<?php echo e($cart['amount']); ?>">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
					<button type="submit" class="btn-xs btn btn-success">Cập Nhật</button>
				</form>
			</td>
			<td>
				<a href="<?php echo e(route('home.deletecart',['id'=>$cart['id']])); ?>"  class="btn-xs btn btn-danger" onclick="return confirm('Bạn có muốn xóa không ?')">Xóa</a>
			</td>
		</tr>
		<?php ($n ++); ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
	</tbody>
	<div>
		<tr>
			<th colspan="3">Tổng số</th>
			<th><?php echo e(number_format($total,0,'',',')); ?> đ</th>
			<th colspan="2"><?php echo e($totalitem); ?> SP	</th>
		</tr>
	</div>
</table>
<div class="text-center">
	<a href="<?php echo e(route('home')); ?>" title="" class="btn btn-success">Tiếp tục mua hàng</a>
	<a href="<?php echo e(route('home.huong-clearcart')); ?>" title="" class="btn btn-danger" onclick="return confirm('Bạn có muốn xóa không ?')">Xóa giỏ hàng</a>
	<a href="<?php echo e(route('home.dathang')); ?>" title="" class="btn btn-warning" >Đặt Hàng</a>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.main-layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>