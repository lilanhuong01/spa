<?php $__env->startSection('title','Quản lý sản phẩm'); ?>
<?php $__env->startSection('content'); ?>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Ảnh</th>
			<th>Tên sản phẩm</th>
			<th>Ngày tạo</th>
			<th>Đường Dẫn</th>
			<th>Trạng thái</th>
			
		</tr>
	</thead>
	<tbody>
	<?php if($datas->count()): ?> <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td>
				<img src="<?php echo e(url('uploads').'/'.$model->image); ?>" width="80" />
			</td>
			<td><?php echo e($model->name); ?></td>
			<td><?php echo e($model->created_at); ?></td>
			<td><?php echo e($model->slag); ?></td>
			<td><?php echo e($model->status); ?></td>
			<td>
				<a href="<?php echo e(route('admin.product-edit',['id'=>$model->id])); ?>" title="Sửa" class="label label-success">
					<i class="fa fa-save"></i> Sửa</a>
					<a href="<?php echo e(route('admin.product-delete',['id'=>$model->id])); ?>" title="Xem" class="label label-danger" onclick="return confirm('Bạn có muốn xóa không?')">Xóa</a>
			</td>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
	</tbody>
</table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>