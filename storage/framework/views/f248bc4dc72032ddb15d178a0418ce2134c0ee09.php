<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]> <html dir="ltr" lang="vi" class="ie8"><![endif]-->
<!--[if IE 9 ]> <html dir="ltr" lang="vi" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="vi">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#FFFFFF">
    <title>NaturalBeauty</title>
   
    <meta property="og:url" content="#">
    <meta property="og:type" content="website">
    <meta property="og:title" content="NaturalBeauty">
    <meta property="og:description">
    <meta property="og:image" content="public/images/LogoSample_ByTailorBrands.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description">
    <meta name="twitter:title" content="NaturalBeauty">
    <meta name="twitter:image" content="public/images/LogoSample_ByTailorBrands.png">
    <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link href='<?php echo e(url("/")); ?>/public/css/stylesheet.min.css' rel="stylesheet">
    <link href='<?php echo e(url("/")); ?>/public/css/owl.carousel.min.css' rel="stylesheet">
  <!--  <link rel="stylesheet" href='<?php echo e(url("/")); ?>/public/css/style.css'> -->
    <script type="text/javascript" src='<?php echo e(url("/")); ?>/public/js/zozo-main.min.js'></script>
    <link href='<?php echo e(url("/")); ?>/public/css/typo.css" type="text/css' rel="stylesheet" media="screen">
    <link href="image\catalog\cart.png" rel="icon">
    <script src='<?php echo e(url("/")); ?>/public/js/jquery.themepunch.plugins.min.js' type="text/javascript"></script>
    <script src='<?php echo e(url("/")); ?>/public/js/jquery.themepunch.revolution.min.js' type="text/javascript"></script>
    <link href='<?php echo e(url("/")); ?>/public/css/stylesheet.css" rel="stylesheet'> </head>
<body class="common-home layout-fullwidth">
    <div class="row-offcanvas row-offcanvas-left">

    <nav id="top" class="topbar-v1" style="background: #212950">
            <div class="container">
                <div class="pull-left">
                    <div class="pull-left">
                        <p style="padding: 13px 0;font-size: 14px;color: #ccc;"><i class="fa fa-phone"></i> Liên hệ: 0968221530</p>
                    </div>
                </div>
                <div class="nav pull-right">
                    <ul class="list-inline">
                        <li class="pull-left hidden-sm hidden-xs">
                            <a href="#">
                                <i class="fa fa-facebook" style="border: 1px solid;width:30px;height:29px;font-size:18px;padding:5px 9px;border-radius: 50%;margin-top:10px"></i>
                            </a>
                        </li>
                        <li class="pull-left hidden-xs">
                            <a href="#">
                                <i class="fa fa-google-plus" style="border: 1px solid;width:30px;height:29px;font-size:18px;padding:5px 7px;border-radius: 50%;margin-top:10px"></i>
                            </a>
                        </li>
                        <li class="pull-left hidden-xs">
                            <a href="#">
                                <i class="fa fa-twitter" style="border: 1px solid;width:30px;height:29px;font-size:18px;padding:5px 7px;border-radius: 50%;margin-top:10px"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
<!-- Menu -->
    <header class="header-v1 bg-white clearfix">
    <div class="container">
        <div class="row">
            <div id="logo" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <a href="index.htm">
                    <img src="<?php echo e(url('/')); ?>/public/images/LogoSample_ByTailorBrands.png" title="Shopspa" alt="Shopspa" class="img-responsive" width="200px"> </a>
                </div>
                <div id="group-menu" class="col-lg-7 col-md-7 col-sm-3 col-xs-2">
                    <div class="bo-mainmenu">
                        <!--===========main menu===========-->
                        <nav id="pav-megamenu" class="navbar">
                            <!--button mobile-->
                            <div class="navbar-header">
                                <button data-toggle="offcanvas" class="btn btn-primary canvas-menu hidden-lg hidden-md" type="button">
                                    <span class="fa fa-bars"></span>
                                </button>
                            </div>
                            <!--menu-->
                            <div class="collapse navbar-collapse" id="bs-megamenu">
                                <ul class="nav navbar-nav megamenu">
                                    <!--menu parent-->
                                    <!--not lv1-->
                                    <li class="">
                                        <a href="<?php echo e(route('home')); ?>">
                                            <span class="menu-title">Trang chủ</span>
                                        </a>
                                    </li>
                                    <!--menu parent-->
                                    <!--not lv1-->
                                    <li class="">
                                        <a href="<?php echo e(route('home.gioithieu')); ?>">
                                            <span class="menu-title">Giới thiệu</span>
                                        </a>
                                    </li>
                                    <!--menu parent-->
                                    <li class="parent dropdown aligned-left">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <span class="menu-title">Dịch Vụ</span>
                                            <b class="caret"></b>
                                        </a>
                                        <div class="dropdown-menu level1">
                                            <div class="dropdown-menu-inner">
                                                <div class="row">
                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-12" data-type="menu">
                                                        <div class="mega-col-inner">
                                                            <ul>
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="dich-vu.php" target="_self">
                                                                        <span class="menu-title">Chăm sóc tóc</span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="dich-vu.php" target="_self">
                                                                        <span class="menu-title">Chăm sóc da</span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="dich-vu.php" target="_self">
                                                                        <span class="menu-title">Làm móng</span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                                <!--menu lv1-->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <!--lv1-->
                                    <li class="parent dropdown aligned-left">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="san-pham.php">
                                            <span class="menu-title">Sản Phẩm</span>
                                            <b class="caret"></b>
                                        </a>
                                        <div class="dropdown-menu level1">
                                            <div class="dropdown-menu-inner">
                                                <div class="row">
                                                    <div class="mega-col col-xs-12 col-sm-12 col-md-12" data-type="menu">
                                                        <div class="mega-col-inner">
                                                            <ul>
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="san-pham.php" target="_self">
                                                                        <span class="menu-title">Chăm sóc tóc</span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="san-pham.php" target="_self">
                                                                        <span class="menu-title">Chăm sóc da</span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="san-pham.php" target="_self">
                                                                        <span class="menu-title">Làm móng</span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                                <!--menu lv1-->
                                                                <!--if not lv2-->
                                                                <li class=" ">
                                                                    <a href="san-pham.php" target="_self">
                                                                        <span class="menu-title">Phụ kiện </span>
                                                                    </a>
                                                                </li>
                                                                <!--end lv1-->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <!--menu parent-->
                                    
                                    <!--not lv1-->
                                    <li class="">
                                        <a href="<?php echo e(route('home.lienhe')); ?>">
                                            <span class="menu-title">Liên hệ</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <!--===========end main menu===========-->
                    </div>
                </div>
                <div id="search" class="col-lg-1 col-md-1 col-sm-6 col-xs-8">
                    <div class="input-group clearfix">
                        <input type="text" name="search" value="" placeholder="Tìm kiếm" class="form-control radius-2x pull-right">
                        <button type="button" class="btn btn-default btn-lg pull-right">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <div id="cart-top" class="col-lg-1 col-md-1 col-sm-3 col-xs-2">
                    <div id="cart" class="dropdown clearfix">
                        <div data-toggle="dropdown" data-loading-text="Đang tải" class="dropdown-toggle">
                            <i class="icon-cart"></i>
                            <h5>SHOPPING CART</h5>
                            <br>
                            <span id="cart-total" class="radius-2x"> 0 </span>
                            <span class="hide-home123"> item(s)</span>
                        </div>
                        <ul class="dropdown-menu bg-white">
                            <li>
                                <p style="padding: 15px 0px;" class="text-center">Giỏ hàng của bạn trống!</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
<!-- Slide -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active" style="background: #1A2661; border-color: #1A2661"></li>
        <li data-target="#myCarousel" data-slide-to="1" style="background: #1A2661; border-color: #1A2661"></li>
        <li data-target="#myCarousel" data-slide-to="2" style="background: #1A2661; border-color: #1A2661"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo e(url('/')); ?>/public/images/banner-nang-nguc-nano-me-hoac-anh-nhin-thang-2-2018.jpg" alt="Los Angeles" style="width:100%; height: 500px">
        </div>
        <div class="item">
            <img src="<?php echo e(url('/')); ?>/public/images/bg-header1.jpg" alt="Chicago" style="width:100%; height: 500px">
        </div>
        <div class="item">
            <img src="<?php echo e(url('/')); ?>/public/images/hoc-phun-xam-thu-mnhap-khung-uu-dai-den-60-phan-tram-tai-thu-cuc.jpg" alt="New York" style="width:100%; height: 500px">
        </div>
    </div>
</div>

<!-- container -->
<h4>Container</h4>

<!-- Footer -->
<footer id="footer">
            <div class="footer-center" id="pavo-footer-center" style="background: #212950">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="widget-html">
                                <div class="widget-inner">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Giới thiệu</h4>
                                    </div>
                                    <p>Ngày nay trong chiến dịch quảng cáo của bất cứ công ty, doanh nghiệp nào cũng không thể thiếu website.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="panel latest-posts latest-posts-v1 panel-default hidden-sm hidden-xs">
                                <div class="panel-heading">
                                    <h4 class="panel-title panel-v1">Dịch Vụ</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="pavblog-latest row">
                                        <div class="col-lg-6 col-md-6 col-xs-12 pav-blog-list">
                                            <div class="blog-item">
                                                <div class="latest-posts-body">
                                                    <div class="latest-posts-img-profile">
                                                        <div class="latest-posts-image effect-adv">
                                                            <a href="quang-cao-khong-co-van-de-neu-ban.htm" class="blog-article">
                                                                <img src="<?php echo e(url('/')); ?>/public/images/blog1-199x153.jpg" title="" class="img-responsive"> </a>
                                                        </div>
                                                        <div class="latest-posts-profile">
                                                            <div class="created">
                                                                <span class="day">13</span>
                                                                <span class="month">Apr</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="latest-posts-meta">
                                                        <h4 class="latest-posts-title">
                                                            <a href="quang-cao-khong-co-van-de-neu-ban.htm" title="Quảng cáo không có vấn đề nếu bạn..">Chăm sóc gia..</a>
                                                        </h4>
                                                        <div class="shortinfo">
                                                            <p> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12 pav-blog-list">
                                            <div class="blog-item">
                                                <div class="latest-posts-body">
                                                    <div class="latest-posts-img-profile">
                                                        <div class="latest-posts-image effect-adv">
                                                            <a href="#" class="blog-article">
                                                                <img src="<?php echo e(url('/')); ?>/public/images/blog2-199x153.jpg" title="Hướng dẫn chọn kem dưỡng da phù hợp cho bạn" alt="" class="img-responsive"> </a>
                                                        </div>
                                                        <div class="latest-posts-profile">
                                                            <div class="created">
                                                                <span class="day">11</span>
                                                                <span class="month">Mar</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="latest-posts-meta">
                                                        <h4 class="latest-posts-title">
                                                            <a href="#" title=""> Phun xăm môi collagen </a>
                                                        </h4>
                                                        <div class="shortinfo">
                                                            <p> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="box">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Sản phẩm Mới nhất</h4>
                                </div>
                                <div class="box-content">
                                    <div class="banner-img">
                                        <a href="tinh-chat-duong-da.htm" class="group1025230239" title="Tinh chất dưỡng da">
                                            <img alt="Tinh chất dưỡng da" src="<?php echo e(url('/')); ?>/public/images/011-77x77.jpg" title="Tinh chất dưỡng da"> </a>
                                        <a href="nen-thom-phong.htm" class="group1025230239" title="Nến thơm phòng">
                                            <img alt="Nến thơm phòng" src="<?php echo e(url('/')); ?>/public/images/012-77x77.jpg" title="Nến thơm phòng"> </a>
                                        <a href="kinh-mat-thoi-trang.htm" class="group1025230239" title="Kính mắt thời trang">
                                            <img alt="Kính mắt thời trang" src="<?php echo e(url('/')); ?>/public/images/013-77x77.jpg" title="Kính mắt thời trang"> </a>
                                        <a href="nuoc-hoa-omnia-crystalline.htm" class="group1025230239" title="Nước hoa Omnia Crystalline">
                                            <img alt="Nước hoa Omnia Crystalline" src="<?php echo e(url('/')); ?>/public/images/014-77x77.jpg" title="Nước hoa Omnia Crystalline"> </a>
                                        <a href="tinh-dau-f-f-no-4html.htm" class="group1025230239" title="Tinh dầu F&amp;F no 4">
                                            <img alt="Tinh dầu F&amp;F no 4" src="<?php echo e(url('/')); ?>/public/images/008-77x77.jpg" title="Tinh dầu F&amp;F no 4"> </a>
                                        <a href="tinh-dau-f-fhtml.htm" class="group1025230239" title="Tinh dầu F&amp;F">
                                            <img alt="Tinh dầu F&amp;F" src="<?php echo e(url('/')); ?>/public/images/002-77x77.jpg" title="Tinh dầu F&amp;F"> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                            <div class="widget-html">
                                <div class="widget-inner">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Liên hệ</h4>
                                    </div>
                                    <ul>
                                        <li class="add">G5 chung cư Five Star,Khương Đình,Thanh Xuân, Hà Nội
                                            <br> </li>
                                        <li class="mobile">0968221530</li>
                                        <li class="mail">lilanhuong01@gmail.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom" id="pavo-footer-bottom">
                <div class="container">
                    <div class="container-inner">
                        <div class="row">
                            <div class="column col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="fb-page" data-href="http://facebook.com/FacebokVietNam" data-tabs="none" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                    <blockquote cite="http://facebook.com/FacebokVietNam" class="fb-xfbml-parse-ignore">
                                        <a href="http://facebook.com/FacebokVietNam">Facebook</a>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="column col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Thông tin</h4>
                                </div>
                                <div class="panel-body">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="gioi-thieu.php">Về chúng tôi</a>
                                        </li>
                                        <li>
                                            <a href="chinh-sach-bao-mat.php">Chính sách bảo mật</a>
                                        </li>
                                        <li>
                                            <a href="thong-tin-giao-hang.php">Thông tin giao hàng</a>
                                        </li>
                                        <li>
                                            <a href="dieu-khoan-va-dieu-kien.php">Điều khoản và điều kiện</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="column col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="pav-newsletter" id="newsletter_912613185">
                                    <form id="formNewLestter912613185" method="post" class="formNewLestter">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Đăng ký nhận tin</h4>
                                        </div>
                                        <div class="description 1">
                                            <p>Đăng ký email để nhanh chóng nhận được các thông báo về khuyến mại, chương trình giảm giá của chúng tôi</p>
                                        </div>
                                        <div class="input-group" style="width: 100%;">
                                            <input type="text" placeholder="Email..." class="form-control email" name="email" id="email">
                                            <button type="submit" name="submitNewsletter" class="btn btn-v1" value="Đăng ký!">Đăng ký!</button>
                                        </div>
                                        <input type="hidden" value="1" name="action">
                                        <div class="valid space-top-10"></div>
                                    </form>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        var id = 'newsletter_912613185';
                                        $('#' + id + ' .box-heading').bind('click', function() {
                                            $('#' + id).toggleClass('active');
                                        });
                                        $('#formNewLestter912613185').on('submit', function() {
                                            var email = $('#formNewLestter912613185 .email').val();
                                            $(".success_inline, .warning_inline, .error").remove();
                                            if (!isValidEmailAddress(email)) {
                                                $('.valid').html("<div class=\"error alert alert-danger\">Email không hợp lệ<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button></div></div>");
                                                $('.email').focus();
                                                return false;
                                            }
                                            var url = "index.php?route=tool/newsletter";
                                            $.ajax({
                                                type: "post",
                                                url: url,
                                                data: $("#formNewLestter912613185").serialize(),
                                                dataType: 'json',
                                                success: function(json) {
                                                    $(".success_inline, .warning_inline, .error").remove();
                                                    if (json['error']) {
                                                        $('.valid').html("<div class=\"warning_inline alert alert-danger\">" + json['error'] + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button></div>");
                                                    }
                                                    if (json['success']) {
                                                        $('.valid').html("<div class=\"success_inline alert alert-success\">" + json['success'] + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button></div>");
                                                    }
                                                }
                                            });
                                            return false;
                                        }); /*end submmit*/
                                    }); /* end document */
                                    function isValidEmailAddress(emailAddress) {
                                        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                                        return pattern.test(emailAddress);
                                    }
                                </script>
                                <ul class="list-inline social-icon-list">
                                    <li>
                                        <a href="http://facebook.com/FacebokVietNam">
                                            <i class="icons icons-color icon-facebook icons-sm fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icons icons-color icon-google-plus icons-sm fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icons icons-color icon-twitter icons-sm fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icons icons-color icon-instagram icons-sm fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright">
            <div class="container">
                <p>&copy; Copyright 2018 Natural Beauty. Thiết kế bởi
                    <a target="_blank" href="webbuilding.xyz">Lan Hương</a>
                </p>
                <img alt="logo" src="<?php echo e(url('/')); ?>/public/images/LogoSample_ByTailorBrands.png"> </div>
        </div>
        <div id="back-to-top">
            <a class="scrollup" href="#">
                <i class="fa fa-angle-up"></i>TOP</a>
        </div>
        <div class="sidebar-offcanvas visible-xs visible-sm">
            <div class="offcanvas-inner panel-offcanvas">
                <div class="offcanvas-heading clearfix">
                    <button data-toggle="offcanvas" class="btn btn-danger pull-right" type="button">
                        <span class="zmdi zmdi-close"></span>
                    </button>
                </div>
                <div class="offcanvas-body">
                    <div id="offcanvasmenu"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $("#offcanvasmenu").html($("#bs-megamenu").html());
        </script>
    </div>
    <!--end .row-offcanvas.row-offcanvas-left-->
    <!-- Facebook script -->
    <div id="fb-root"></div>
    <script type="text/javascript">
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=829732533863539";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Google Plus script -->
    <script type="text/javascript">
        ! function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + '://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, 'script', 'twitter-wjs');
    </script>
    <!-- Google Plus script -->
    <script type="text/javascript">
        window.___gcfg = {
            lang: 'vi'
        };
        (function() {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
    </script>
    <!-- Zalo -->
    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
</body>

</html>